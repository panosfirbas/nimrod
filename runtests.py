import unittest
import traceback
from os import path,getcwd
import sys

# import nimrod's main function so we can test it
from nimrod.__main__ import main as nimain

import warnings
import zipfile


from nimrod.arg_parser_funcs import *
class O():
    pass


# from https://www.neuraldump.net/2017/06/how-to-suppress-python-unittest-warnings/
# a wrapper to ignore warnings in unittesting
def ignore_warnings(test_func):
    def do_test(self, *args, **kwargs):
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            # warnings.filterwarnings("ignore", category=ResourceWarning)
            test_func(self, *args, **kwargs)
    return do_test






# one class per tested function
class Test_Main(unittest.TestCase):

    @ignore_warnings
    def test_thatItLoads(self):
        try:
            args = O()
            args.myfunc = 'pass'
            nimain(True, args)
        except Exception as e:
            self.fail(f"main failed to load unexpectedly!\n\n{traceback.format_exc()}")


# 
class Test_training_manager(unittest.TestCase):
    def __init__(self, methodName="runTest"):
        unittest.TestCase.__init__(self, methodName)
        self.args = O()
        self.TEMPDIR = tempfile.TemporaryDirectory()
        self.args.out = path.join(self.TEMPDIR.name, 'training_test_folder')
        self.args.ndata = path.join(RUNNINGDIR,"test_data","test_preproc")
        self.args.bsm = 12
        self.args.dexe = 1000
        self.args.epochs = 2
        self.args.test = False
        self.args.valfre = 20
        self.args.patience = 10
        self.args.slr = 1e-06
        self.args.weights = [1., 1.]
        self.args.model = None
        self.args.float_printables = ['loss', 'auc','aps','F05','F1','F2','Precision','youden', 'sensitivity', 'specificity', 'fpr']
        self.args.not_float_printables = ['time','epoch','ustep','willSaveModelState','lrnRate']

    def __del__(self):
        self.TEMPDIR.cleanup()

    @ignore_warnings
    def test_thatItLoads2(self):
        try:
            training_manager(self.args)
        except Exception as e:
            self.fail(f"training_manager failed to load unexpectedly!\n\n{traceback.format_exc()}")
        pass






class Test_makedataset(unittest.TestCase):
    def __init__(self, methodName="runTest"):
        unittest.TestCase.__init__(self, methodName)

        self.args = O()
        self.TEMPDIR = tempfile.TemporaryDirectory()
        import json

        data = {"chip_peaks" : {'stage1'  : path.join(RUNNINGDIR,"test_data","test_chipPeaks.narrowPeak"),
                  'stage2'  : path.join(RUNNINGDIR,"test_data","test_chipPeaks.narrowPeak"),
                  },
            "atac_peaks" : {'stage1': path.join(RUNNINGDIR,"test_data","test_atacPeaks.narrowPeak"),
                            'stage2': path.join(RUNNINGDIR,"test_data","test_atacPeaks.narrowPeak"),
                            },

            "genomic_pwm" : path.join(RUNNINGDIR,"test_data","test_genomicCTCF.bed")
            }
        
        self.args.json = path.join(self.TEMPDIR.name, 'test_makedataset.json',)# path to a json MAKE ONE, REQUIRED
        with open(self.args.json, "w") as write_file:
            json.dump(data, write_file)

        self.args.out = path.join(self.TEMPDIR.name, "test_makedataset.out")        
        self.args.aresorted = False
        self.args.sortthem =True #try with and without
        self.args.nochip = False# try with and without
        self.args.setsglobal =False # try with and without
        self.args.tempdir = "/tmp" # make second temp folder
        self.args.printjson =False # try with and without
        self.args.f_atac = 0.5
        self.args.f_chip = 0.5

    def __del__(self):
        self.TEMPDIR.cleanup()

    @ignore_warnings
    def test_thatItLoads(self):
        try:
            pass
            makedataset(self.args)

        except Exception as e:
            self.fail(f"makedataset failed to load unexpectedly!\n\n{traceback.format_exc()}")
        pass

class Test_make_bws(unittest.TestCase):
    def __init__(self, methodName="runTest"):
        unittest.TestCase.__init__(self, methodName)

        self.args = O() 
        self.TEMPDIR = tempfile.TemporaryDirectory()

        data = {"stage1": [path.join(RUNNINGDIR,"test_data","test.bam")]}
        self.args.json = path.join(self.TEMPDIR.name, 'test_makebw.json')# path to a json MAKE ONE, REQUIRED
        with open(self.args.json, "w") as write_file:
            json.dump(data, write_file)

        self.args.out = path.join(self.TEMPDIR.name, 'test_bw_folder',)
        self.args.chrinfo = path.join(RUNNINGDIR,"test_data","hg38.chrinfo")
        self.args.procs = 4
        self.args.verbose = False
        self.args.printjson = False
    
    def __del__(self):
        self.TEMPDIR.cleanup()

    @ignore_warnings
    def test_thatItLoads(self):
        try:
            make_bws(self.args)
        except Exception as e:
            self.fail(f"make_bws failed to load unexpectedly!\n\n{traceback.format_exc()}")
        pass



class Test_mkgenomeindex(unittest.TestCase):
    def __init__(self, methodName="runTest"):
        unittest.TestCase.__init__(self, methodName)
        self.args = O()
        self.TEMPDIR = tempfile.TemporaryDirectory()
        self.args.fa = path.join(RUNNINGDIR,"test_data","test.fa")
        self.args.out = path.join(self.TEMPDIR.name, 'test_makegenome.pickle',)

    def __del__(self):
        self.TEMPDIR.cleanup()
    
    @ignore_warnings
    def test_thatItLoads(self):
        try:
            mkgenomeindex(self.args)
        except Exception as e:
            self.fail(f"mkgenomeindex failed to load unexpectedly!\n\n{traceback.format_exc()}")
        pass

class Test_mk_chrinfo(unittest.TestCase):
    def __init__(self, methodName="runTest"):
        unittest.TestCase.__init__(self, methodName)
        self.args = O()
        self.TEMPDIR = tempfile.TemporaryDirectory()

        self.args.out = path.join(self.TEMPDIR.name, 'test_chromimfo.txt',)

    def __del__(self):
        self.TEMPDIR.cleanup()
    
    @ignore_warnings   
    def test_thatItLoads(self):
        try:
            self.args.genome = path.join(RUNNINGDIR,"test_data","test.fa")
            mk_chrinfo(self.args)
        except Exception as e:
            self.fail(f"mk_chrinfo failed to load unexpectedly!\n\n{traceback.format_exc()}")
        pass

    @ignore_warnings   
    def test_thatItLoads2(self):
        try:
            self.args.genome = path.join(RUNNINGDIR,"test_data","test_hg38.pickle") 
            mk_chrinfo(self.args)
        except Exception as e:
            self.fail(f"mk_chrinfo failed to load unexpectedly!\n\n{traceback.format_exc()}")
        pass


class Test_mkchunks_manager(unittest.TestCase):
    def __init__(self, methodName="runTest"):
        unittest.TestCase.__init__(self, methodName)
        self.args = O()
        self.TEMPDIR = tempfile.TemporaryDirectory()

        self.args.genome = path.join(RUNNINGDIR,"test_data","test_hg38.pickle")         
        self.args.bwf = path.join(RUNNINGDIR,"test_data","test_bw_folder") 
        self.args.df =  path.join(RUNNINGDIR,"test_data","test_dataset.tsv") 
        self.args.out = path.join(self.TEMPDIR.name, 'test_preprocfolder',)
        self.args.win_size = 1000

    def __del__(self):
        self.TEMPDIR.cleanup()
    @ignore_warnings   
    def test_thatItLoads(self):
        try:
            mkchunks_manager(self.args)
        except Exception as e:
            self.fail(f"mkchunks_manager failed to load unexpectedly!\n\n{traceback.format_exc()}")
        pass









class Test_continue_training_manager(unittest.TestCase):
    def __init__(self, methodName="runTest"):
        unittest.TestCase.__init__(self, methodName)
        self.args = O()
        self.TEMPDIR = tempfile.TemporaryDirectory()

        self.args.genome = path.join(RUNNINGDIR,"test_data","test_hg38.pickle")         
        # self.args.df =  path.join(RUNNINGDIR,"test_data","test_dataset.tsv") 
        self.args.out = path.join(self.TEMPDIR.name, 'test_testresultsfolder')


        self.args.modelPdir = path.join(RUNNINGDIR, 'test_data','model')
        self.args.modelP = path.join(self.args.modelPdir,'model_state_lastState-2115000' )
        self.args.ndata = path.join(RUNNINGDIR,"test_data","test_preproc")


        self.args.weights = [1., 1.]
        self.args.win_size = 1000
        self.args.cache_chunks = '010'
        self.args.test = True

        self.args.epochs = 2
        self.args.valfre = 100
        self.args.patience = 1
        self.args.slr = 1e-06
        self.args.dexe = 1000
        self.args.bsm = 9
        self.args.model = None
        self.args.not_float_printables = ['time','epoch','ustep','willSaveModelState','lrnRate']
        self.args.float_printables = ['loss', 'auc','aps','F05','F1','F2','Precision','youden', 'sensitivity', 'specificity', 'fpr']


    def __del__(self):
        # rezip the model files to save some space
        self.TEMPDIR.cleanup()
    
    # def unzip_file(self, target_file, target_folder):
    #     with zipfile.ZipFile(target_file, 'r') as zip_ref:
    #         zip_ref.extractall(target_folder)
    # def zip_file(self, target_file):
    #     # writing files to a zipfile 
    #     with zipfile.ZipFile(target_file,'w') as zip: 
    #         zip.write(target_file[:-3]) 

    @ignore_warnings   
    def test_thatItLoads(self):
        try:
            continue_training_manager(self.args)
        except Exception as e:
            self.fail(f"continue_training_manager failed to load unexpectedly!\n\n{traceback.format_exc()}")
        pass














# class Test_printjson(unittest.TestCase):
#     def __init__(self, methodName="runTest"):
#         unittest.TestCase.__init__(self, methodName)
#         self.args = O()
        
#     # def __del__(self):
#     #     # rezip the model files to save some space
#     #     self.TEMPDIR.cleanup()

#     @ignore_warnings   
#     def test_thatItLoads(self):
#         try:
#             printjson(self.args)
#         except Exception as e:
#             self.fail(f"printjson failed to load unexpectedly!\n\n{traceback.format_exc()}")
#         pass




class Test_kowalski(unittest.TestCase):
    def __init__(self, methodName="runTest"):
        unittest.TestCase.__init__(self, methodName)
        self.args = O()
        self.args.genome = path.join(RUNNINGDIR,"test_data","test_hg38.pickle")         
        self.args.df =  path.join(RUNNINGDIR,"test_data","test_dataset.tsv") 
    
    @ignore_warnings   
    def test_thatItLoads(self):
        try:
            kowalski(self.args)
        except Exception as e:
            self.fail(f"kowalski failed to load unexpectedly!\n\n{traceback.format_exc()}")
        pass
















# deprecated
# class Test_manual_lr_manager(unittest.TestCase):
#     def __init__(self, methodName="runTest"):
#         unittest.TestCase.__init__(self, methodName)
#         self.args = O()
        
#     # def __del__(self):
#     #     # rezip the model files to save some space
#     #     self.TEMPDIR.cleanup()

#     @ignore_warnings   
#     def test_thatItLoads(self):
#         try:
#             manual_lr_manager(self.args)
#         except Exception as e:
#             self.fail(f"manual_lr_manager failed to load unexpectedly!\n\n{traceback.format_exc()}")
#         pass









if __name__ == '__main__':
    import tempfile

    RUNNINGDIR = getcwd()
    
    unittest.main()


    

    