
import numpy as np
from multiprocessing import Pool
from os import path
import SharedArray as sha
import shutil
import itertools
from functools import partial


from .helpers import make_bw_dict,chooser,reverse,complement



def my_pool_init_func( p_genomeD, BWFOLDERPATH ):

    # It looks like the pyBigWig objects CANNOT
    # be initialized before this step and passed as an argument
    # they HAVE TO be initialized in here
    # If not, I get 
    # "[bwGetOverlappingIntervalsCore] Got an error"
    # or 
    # "RuntimeError: An error occurred while fetching values!"
    # which seems like a racing problem ?
    # After plenty of testing it felt stochastic so im going with racing issue
    bwD = make_bw_dict(BWFOLDERPATH)

    global glob_bw_dict
    glob_bw_dict = bwD
    global genomeD
    genomeD = p_genomeD

def atac_signal_window_constructor(stage, chrom, start, end, strand, ctd=0.666):
    choicelist = chooser(glob_bw_dict[stage], ctd)
    atac_wi = np.zeros((end-start, 3, 2))
    for thing in choicelist:

        try:
            # get FrC1_p,FrC1_n and put them in the appropriate 
            c1p = np.array(thing['FrC1']['p'].values(chrom, start, end))
            c1p[np.isnan(c1p)] = 0
            if len(c1p)==0: # in the yeast experiments, some bw files seem to be malformed
            # they return an empty list. We treat them as empty and replace with zeros
            # this only seemed to happen in the first fragment length
                c1p = np.zeros(atac_wi.shape[0])
            atac_wi[:,0,0] += c1p
            c1n = np.array(thing['FrC1']['n'].values(chrom, start, end))
            c1n[np.isnan(c1n)] = 0
            if len(c1n)==0: # in the yeast experiments, some bw files seem to be malformed
            # they return an empty list. We treat them as empty and replace with zeros
            # this only seemed to happen in the first fragment length
                c1n = np.zeros(atac_wi.shape[0])
            atac_wi[:,0,1] += c1n

            c2p = np.array(thing['FrC2']['p'].values(chrom, start, end))
            c2p[np.isnan(c2p)] = 0
            if len(c2p)==0: # in the yeast experiments, some bw files seem to be malformed
            # they return an empty list. We treat them as empty and replace with zeros
            # this only seemed to happen in the first fragment length
                c2p = np.zeros(atac_wi.shape[0])
            atac_wi[:,1,0] += c2p
            c2n = np.array(thing['FrC2']['n'].values(chrom, start, end))
            c2n[np.isnan(c2n)] = 0
            if len(c2n)==0: # in the yeast experiments, some bw files seem to be malformed
            # they return an empty list. We treat them as empty and replace with zeros
            # this only seemed to happen in the first fragment length
                c2n = np.zeros(atac_wi.shape[0])
            atac_wi[:,1,1] += c2n
            
            c3p = np.array(thing['FrC3']['p'].values(chrom, start, end))
            c3p[np.isnan(c3p)] = 0
            if len(c3p)==0: # in the yeast experiments, some bw files seem to be malformed
            # they return an empty list. We treat them as empty and replace with zeros
            # this only seemed to happen in the first fragment length
                c3p = np.zeros(atac_wi.shape[0])
            atac_wi[:,2,0] += c3p
            c3n = np.array(thing['FrC3']['n'].values(chrom, start, end))
            c3n[np.isnan(c3n)] = 0
            if len(c3n)==0: # in the yeast experiments, some bw files seem to be malformed
            # they return an empty list. We treat them as empty and replace with zeros
            # this only seemed to happen in the first fragment length
                c3n = np.zeros(atac_wi.shape[0])
            atac_wi[:,2,1] += c3n
        except RuntimeError:
            print("RUNTIME ERROR: ",chrom, start, end,
               sep='\t' )
    
    if strand == '-':
        atac_wi = atac_wi[::-1,:,:]
    return atac_wi

def worker(argument_tuple, chanceToDrop):
    labelD = {0 : [1,0], 
          1 : [0,1] }
    rowindex,row = argument_tuple
    # Get the atac signal
    atac_wi = atac_signal_window_constructor(row.stage, row.chrom, row.start, row.end, row.strand, chanceToDrop)
    # Get the sequence signal (in one-hot)
    intD = {'A' :0,'C':1,'G':2,'T':3,
            'a': 0,'c':1,'g':2,'t':3,'N':4,'n':4}
    # intD = {'A' :0,'C':1,'G':2,'T':3,
    #         'a': 4,'c':5,'g':6,'t':7,'N':8,'n':9}
    eye_ = np.eye(5, dtype=np.uint8 )
    raw_seq = genomeD[row.chrom][row.start:row.end]
    if len(raw_seq) != row.end-row.start:
        raw_seq = 'n'*(row.end-row.start)
    if row.strand == '-':
        raw_seq = reverse(complement(raw_seq))
    seqint = [intD[x] for x in raw_seq]
    seq_wi = eye_[seqint]
    
    del raw_seq, seqint,eye_
    return rowindex, labelD[row.iT], atac_wi, seq_wi

worker0 = partial(worker, chanceToDrop=0)
worker066 = partial(worker, chanceToDrop=0.666)

def grouper(n, iterable):
    it = iter(iterable)
    while True:
        chunk = tuple(itertools.islice(it, n))
        if not chunk:
            return
        if len(chunk) !=n:
            return
        yield chunk


def chunkify_array(a,chunksize ):
    new_first_dim = int(a.shape[0] / chunksize)
    # np.lib.stride_tricks.as_strided(a, new_array_shape, new_array_strides)
    # new_array_shape = new_first_dim,chunksize,*therestofdimentions
    # new_array_strides = chunksize * a.strides[0], *therestofthestrides

    yield from np.lib.stride_tricks.as_strided(a, (new_first_dim,chunksize,*a.shape[1:]), 
                                        (chunksize * a.strides[0], *a.strides))

def chunkify_array_withchange(a,chunksize ):
    '''returns the remainder items after chunkifying,
    doest work in training cause model expects specific
    dimenions, but we need it in tests to test all the
    samples'''
    new_first_dim = int(a.shape[0] / chunksize)
    yield from np.lib.stride_tricks.as_strided(a, (new_first_dim,chunksize,*a.shape[1:]), 
                                        (chunksize * a.strides[0], *a.strides))

    yourchange = a.shape[0] - (new_first_dim*chunksize)
    yield a[-yourchange:]

def make_chunks_sub(theargs, df, funk, genomeD, name):
    df = df.reset_index(drop=True)

    asizevar= len(df)
    ch_a = np.zeros( (asizevar,) , dtype=np.uint32)
    ch_b = np.zeros( (asizevar,2) , dtype=np.uint8)
    ch_c = np.zeros( (asizevar,1000,3,2) , dtype=np.uint8)
    ch_d = np.zeros( (asizevar,1000,5) , dtype=np.uint8)

    with Pool(processes=5,maxtasksperchild=50, initializer=my_pool_init_func, initargs=(genomeD, theargs.bwf, ) ) as pool:
        for _a, _b, _c, _d in pool.imap_unordered( funk, df.iterrows(), chunksize=500 ):
            ch_a[_a] = _a
            ch_b[_a] = _b
            ch_c[_a] = _c
            ch_d[_a] = _d

    ch_a_fp= path.join(theargs.savefolder, "shm_array_{}_a".format(name)) 
    ch_b_fp= path.join(theargs.savefolder, "shm_array_{}_b".format(name)) 
    ch_c_fp= path.join(theargs.savefolder, "shm_array_{}_c".format(name)) 
    ch_d_fp= path.join(theargs.savefolder, "shm_array_{}_d".format(name)) 

    ch_a_sha = sha.create("file://"+ch_a_fp, ch_a.shape, dtype=np.uint32)
    ch_b_sha = sha.create("file://"+ch_b_fp, ch_b.shape, dtype=np.uint8)
    ch_c_sha = sha.create("file://"+ch_c_fp, ch_c.shape, dtype=np.uint8)
    ch_d_sha = sha.create("file://"+ch_d_fp, ch_d.shape, dtype=np.uint8)
    
    ch_a_sha[:] = ch_a
    ch_b_sha[:] = ch_b
    ch_c_sha[:] = ch_c
    ch_d_sha[:] = ch_d

    del ch_a_sha
    del ch_a
    del ch_b_sha
    del ch_b
    del ch_c_sha
    del ch_c
    del ch_d_sha
    del ch_d

    filepaths = ch_a_fp, ch_b_fp, ch_c_fp, ch_d_fp
    return filepaths


def activate_chunks(folder, name):
    
    ch_a_fp= path.join(folder, "shm_array_{}_a".format(name)) 
    ch_b_fp= path.join(folder, "shm_array_{}_b".format(name)) 
    ch_c_fp= path.join(folder, "shm_array_{}_c".format(name)) 
    ch_d_fp= path.join(folder, "shm_array_{}_d".format(name)) 

    if path.isfile(ch_a_fp):

        shutil.copy( ch_a_fp, "/dev/shm/")
        shutil.copy( ch_b_fp, "/dev/shm/")
        shutil.copy( ch_c_fp, "/dev/shm/")
        shutil.copy( ch_d_fp, "/dev/shm/")

    else:

        print(" WARNING/ERROR: Chunks for {} not found, check that the chunk preprocessing has been done properly,\
            and that you are passing the proper folder as input argument".format(name))


class ChunkManager():
    def __init__(self, chunk_size, bsm, input_dataframe, minibatch=False, ):
        self.chunk_size=chunk_size
        self.bsm = bsm

        self.init_chunk_size=chunk_size
        self.df = input_dataframe
        self.df = self.df.reset_index(drop=True)
        
        self.minibatch = minibatch
        if self.minibatch:
            self.make_minibatch_numbers()


    def make_minibatch_numbers(self,):
        """They numbers for making a minibatch won't change from one call toactivate_chunks
        another, so we only need to compute them once"""
        df = self.df
        self.positive_index_positions = df[df.iT==1].index.values
        self.negative_index_positions = df[df.iT==0].index.values

        self.dfPositives = df.iT.sum()
        self.dfNegatives = len(df) - self.dfPositives

        self.n2p = float(self.dfNegatives / self.dfPositives)

        if self.n2p <1:
            # more negatives than positives
            positives_in_each_batch = self.bsm
            negatives_in_each_batch = int(positives_in_each_batch * self.n2p)
        else:
            negatives_in_each_batch = self.bsm
            positives_in_each_batch = int(negatives_in_each_batch * (1/self.n2p))


        total_in_batch = positives_in_each_batch + negatives_in_each_batch

        self.how_many_batches_does_that_make = int(len(df)/total_in_batch)

        self.HowManyPositivesIllNeed = positives_in_each_batch * self.how_many_batches_does_that_make
        self.HowManyNegativesIllNeed = negatives_in_each_batch * self.how_many_batches_does_that_make


        print(">>>>>>>>>> lendf: {}".format(len(df)))
        print(">>>>>> #Positives {}".format(self.dfPositives))
        print(">>>>>> #Negatives {}".format(self.dfNegatives))
        print(">>>>>> n2p {}".format(self.n2p))

        print(">>>>>>>>>> positives_in_each_batch: {}".format(positives_in_each_batch))
        print(">>>>>>>>>> negatives_in_each_batch: {}".format(negatives_in_each_batch))
        print(">>>>>>>>>> how_many_batches_does_that_make: {}".format(self.how_many_batches_does_that_make))
        print(">>>>>>>>>> HowManyPositivesIllNeed {} ".format(self.HowManyPositivesIllNeed))
        print(">>>>>>>>>> HowManyNegativesIllNeed {} ".format(self.HowManyNegativesIllNeed))
        
        #how many positives go into each chunk?
        self.minibatch_negativesNum = negatives_in_each_batch
        self.minibatch_positivesNum = positives_in_each_batch

        # override this value when we make minibatches
        if self.bsm != False:
            self.chunk_size = negatives_in_each_batch + positives_in_each_batch

        else:
            basechunksize = (self.minibatch_negativesNum + 1)
            self.chunk_size = int(self.init_chunk_size / basechunksize) * basechunksize

        # print("I determinded the batch size at {}".format(self.chunk_size), flush=True)  

    def make_a_minibatch_index(self,):
        
        rand_positive_index_positions = np.random.permutation(self.positive_index_positions)[:self.HowManyPositivesIllNeed]
        rand_negative_index_positions = np.random.permutation(self.negative_index_positions)[:self.HowManyNegativesIllNeed]

        theindex = [a+b for a,b in 
            zip(grouper(self.minibatch_positivesNum, rand_positive_index_positions), 
                grouper(self.minibatch_negativesNum, rand_negative_index_positions)
               )]
        theindex = np.array([x for y in theindex for x in y])
        return theindex

def give_me_chunks( name, chunksize, minibatch = False):
    # print("attaching to {}".format(name))
    ch_a,ch_b,ch_c,ch_d = attach_chunks(name)
    # print("minibtching {}".format(name))
    if type(minibatch) == np.ndarray:
        mb = minibatch

        ch_a = ch_a[mb]
        ch_b = ch_b[mb]
        ch_c = ch_c[mb]
        ch_d = ch_d[mb]
    # print("yielding {}".format(name))
    yield from zip( chunkify_array( ch_a, chunksize ),
                    chunkify_array( ch_b, chunksize ),
                    chunkify_array( ch_c, chunksize ),
                    chunkify_array( ch_d, chunksize )
                    )
    # print("deleting {}".format(name))
    del ch_a
    del ch_b
    del ch_c
    del ch_d



def give_me_chunks_withchange( name, chunksize):
    '''returns the remainder items after chunkifying,
    doest work in training cause model expects specific
    dimenions, but we need it in tests to test all the
    samples'''
    # print("attaching to {}".format(name))
    ch_a,ch_b,ch_c,ch_d = attach_chunks(name)
    # print("yielding {}".format(name))
    yield from zip( chunkify_array_withchange( ch_a, chunksize ),
                    chunkify_array_withchange( ch_b, chunksize ),
                    chunkify_array_withchange( ch_c, chunksize ),
                    chunkify_array_withchange( ch_d, chunksize )
                    )
    # print("deleting {}".format(name))
    del ch_a
    del ch_b
    del ch_c
    del ch_d

def attach_chunks(name):
    ch_a_shmfp= "shm_array_{}_a".format(name)
    ch_b_shmfp= "shm_array_{}_b".format(name)
    ch_c_shmfp= "shm_array_{}_c".format(name)
    ch_d_shmfp= "shm_array_{}_d".format(name)

    ch_a = sha.attach(ch_a_shmfp)
    ch_b = sha.attach(ch_b_shmfp)
    ch_c = sha.attach(ch_c_shmfp)
    ch_d = sha.attach(ch_d_shmfp)

    return ch_a,ch_b,ch_c,ch_d


def delete_chunks(name):
    ch_a_shmfp= "shm_array_{}_a".format(name)
    ch_b_shmfp= "shm_array_{}_b".format(name)
    ch_c_shmfp= "shm_array_{}_c".format(name)
    ch_d_shmfp= "shm_array_{}_d".format(name)

    ch_a = sha.delete(ch_a_shmfp)
    ch_b = sha.delete(ch_b_shmfp)
    ch_c = sha.delete(ch_c_shmfp)
    ch_d = sha.delete(ch_d_shmfp)