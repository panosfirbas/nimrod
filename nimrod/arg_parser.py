import argparse
import sys
import os
import pickle
from .b2bw import make_the_parser as b2bw_arg_parser
from .b2bw import parse_args as b2bw_parse_args
from .b2bw import bamtobw

import pandas as pd
import numpy as np
from multiprocessing import Pool

def parse_arguments():
    # # create the top-level parser
    parser = argparse.ArgumentParser(prog='nimrod',
        description="Nimrod is a model implemented in \
        TensorFlow. This package helps with the necessary \
        data processing, and allows training of the model \
        and using a pretrained model to run tests.\
        To train a model, you will need to prepare a genome.pickle,\
        a dataset.tsv and a prep folder, using the 'nimrod prep' functions. \
        Read the repo's README for more inrofmation\
        https://gitlab.com/panosfirbas/nimrod",
        epilog="",
        )

    subparsers = parser.add_subparsers(title='subcommands',metavar='',help='')
    # modules = parser.add_subparsers(dest='modules',title="Available modules",description="",help='',metavar="")

    
    aux = subparsers.add_parser('aux', help='Auxiliary functions',
        description="to-do")
    aux_subs = aux.add_subparsers(title="Available functions", description="", help='', metavar="")


    # Print mock json file
    #######################################################################################################################################

    # parser_printjson = aux_subs.add_parser('json_forbw', description = "Outputs an example json file, in the format that is needed to make the bigwig preprocessing.",
    #     help = "Outputs an example json file, in the format that is needed to make the bigwig preprocessing." )
    # # this would have been nice to use, but I can't get it to import TF when I want it to, which
    # # ends up importing TF during the command line help which sucks.
    # # parser_printjson.set_defaults(func=mkgenomeindex)
    # parser_printjson.add_argument('--myfunc',  default="printjson", help=argparse.SUPPRESS) 


    parser_moods = aux_subs.add_parser('moods', description = "Converts a moods csv output to a normal bed file",
        help = "Converts a moods csv output to a normal bed file" )
    parser_moods.add_argument('--csv', help='path to a csv output file of moods', metavar='FILE', required=True)
    parser_moods.add_argument('--out', help='path to the tsv output file', metavar='FILE', required=True)
    parser_moods.add_argument('--tfname', help='The name of the transcription factor, something like "CTCF"', metavar='FILE', required=True)

    parser_moods.add_argument('--myfunc',  default="moods2tsv", help=argparse.SUPPRESS) 


    # Analyze df to determine proper bsm
    #######################################################################################################################################
    parser_kowalski = aux_subs.add_parser('bsm_analysis', description = "You will need to choose a batch size multiplier to train your model.\
        A good starting point, is to run this command, and choose the smallest bsm value that gives a very small 'ratio'(the 4th column). \
        This will be the batch size that best replects the ratio of positives to negatives in the full set.",
        help = "Runs a simple analysis on different bsm values for a given dataset." )
    parser_kowalski.add_argument('--df', help='path to a table with the motif hits\
        formatted properly', metavar='FILE', required=True)
    parser_kowalski.add_argument('--genome', help='path to a genome file (.fa or .pickle)', metavar='FILE', required=False)
    
    parser_kowalski.add_argument('--myfunc',  default="kowalski", help=argparse.SUPPRESS)


    # # Analyze Learning rate
    # #######################################################################################################################################
    # parser_lr = subparsers.add_parser('lrtrain')

    # parser_lr.add_argument('--genome', help='path to a genome file (.fa or .pickle)', metavar='FILE', required=False)
    # # parser_lr.add_argument('--df',     help='path to a table with the motif hits, stage, isReal, set', metavar='FILE', required=False)
    # parser_lr.add_argument('--prep',   help="path to folder created with the 'makechunks' command ",  metavar='FOLDER', default="NOPE")
    # parser_lr.add_argument('--out',    help='path to output folder', metavar='FOLDER', required=True)
    
    # parser_lr.add_argument('--epochs', help='maximum number of epochs to run', metavar='NUM',type=int , default=300)
    # parser_lr.add_argument('--valfre',  metavar='NUM',type=int, default=5000 , help="""Validation Freequency: Every how many batches should I test the validation set?""")
    # parser_lr.add_argument('--patience', metavar='NUM',type=int, default=10 , help="""If we don't improve after this many validation tests, stop the training """)

    # parser_lr.add_argument('--slr', type=float, default=0.00001, help="Starting Learning Rate.")
    # parser_lr.add_argument('--dexe', type=float, default=5, help="The learning rate decays to LR*0.9 every *dexe* epochs")
    # parser_lr.add_argument('--bsm', type=int, default=2, help="batch size multiplier. Try values from 2-50+")

    
    # # batch size multiplier: if we have 1positive:Xnegatives,
    # # the minibatchsize during training will be bsm*(X+1)
    # parser_lr.add_argument('--model', default=None,metavar='FILE', help=argparse.SUPPRESS)
    # parser_lr.add_argument('--weights', metavar='NUM',type=float, nargs='+', default=[1., 1.] , help=argparse.SUPPRESS)
    # parser_lr.add_argument('--bwf', help=argparse.SUPPRESS, metavar='FOLDER', required=False)
    # parser_lr.add_argument('--test', action='store_true', default=False, help=argparse.SUPPRESS)
    # parser_lr.add_argument('--win_size', type=int, default=1000, help=argparse.SUPPRESS)
    
    # # this would have been nice to use, but I can't get it to import TF when I want it to, which
    # # ends up importing TF during the command line help which sucks.
    # # parser_lr.add_argument('--cache_chunks', choices=['000','001','010','100','011','110','101','111'], default='011', help=argparse.SUPPRESS)
    # parser_lr.add_argument('--myfunc',  default="manual_lr_manager", help=argparse.SUPPRESS) 




    prep = subparsers.add_parser('prep', help='Data preparation functions',
        description="to-do")
    prep_subs = prep.add_subparsers(title="Available functions", description="", help='', metavar="")
    # makedataset
    #######################################################################################################################################
    parser_dataset = prep_subs.add_parser('dataset', 
        description='Nimrod expects the putative PWM sites that will be used to train or test\
        in a specific format. This subcommand will help you create the dataset the way that \
        the tool expects it.\
        You will need to construct a .json file that points to your PWMs and peaks. \
        Use the --printjson to print an example.',
        help = "DataPreparation 1/4: Script to help you create a dataset the way Nimrod expects it.",
        epilog='example usage: "nimrod prep dataset --json ./makedataset.json --out ./nimrod_dataset.tsv" ' )

    
    # parser_dataset.add_argument('--genome', help='path to a genome file (.fa or .pickle)', metavar='FILE', required=True)
    parser_dataset.add_argument('--json', help='path to a .json file containing paths to your bedfiles', metavar='JSONFILE', required=False)
    parser_dataset.add_argument('--printjson', help='Print an example of the .json file expected in the --json argument.', action='store_true', default=False, required=False)
    
    parser_dataset.add_argument('--out',    help='path to output folder', metavar='FOLDER', required=False)
    
    parser_dataset.add_argument('--aresorted', action='store_true', default=False, help="Set to True if your bedfiles are already sorted.")
    parser_dataset.add_argument('--sortthem', action='store_true', default=False, help="Set to True if you want to sort your bedfiles\
        before intersecting (useful if your bedfiles are huge).")
    parser_dataset.add_argument('--tempdir', help='Path to folder where some temporary files will be created, default "/tmp".\
        Point it to a scratch/fast disk partition to speed things up.', default="/tmp", metavar='TEMPDIR', required=False)      
        

    
    parser_dataset.add_argument('--f_atac',  metavar='NUM',type=float, default=1E-9 , help="The 'bedtools intersect' f argument, controls what percentage of a PWM hit must overla\
    an atac peak in order to be counted. ")
    parser_dataset.add_argument('--f_chip',  metavar='NUM',type=float, default=1E-9 , help="The 'bedtools intersect' f argument, controls what percentage of a PWM hit must overla\
    a chip peak in order to be counted. ")
    # parser_dataset.add_argument('--justtest', action='store_true', default=False, help="Set if you want all your pwms to be treated as a test")
    parser_dataset.add_argument('--nochip', action='store_true', default=False, help="Set this if you don't have chipseq experiments and just \
        want to create a test dataset.")
    parser_dataset.add_argument('--setsglobal', action='store_true', default=False, help="Set if you want the train-validation-test sampling\
        to be done globally instead of per stage.")


    parser_dataset.add_argument('--myfunc',  default="makedataset", help=argparse.SUPPRESS)     
    

    # Make a genome index
    #######################################################################################################################################
    parser_mkgenindex = prep_subs.add_parser('genome', help='DataPreparation 2/4: Pickle a genome file.\
        Do this once per genome.',
        description='Nimrod needs a genome to extract the sequence from.\
        This command creates a genome file (yourgenome.pickle) which is much faster to load than the traditional fasta files.\
        Do this once per genome.',
        epilog='example usage: "nimrod prep genome --fa hg19.fa --out hg19.pickle" ' )
    parser_mkgenindex.add_argument('--fa', help='path to a genome fa file', metavar='')
    parser_mkgenindex.add_argument('--out', help='output file name', metavar='')
    
    # this would have been nice to use, but I can't get it to import TF when I want it to, which
    # ends up importing TF during the command line help which sucks.
    # parser_mkgenindex.set_defaults(func=mkgenomeindex)
    parser_mkgenindex.add_argument('--myfunc',  default="mkgenomeindex", help=argparse.SUPPRESS) 

    # Make a chrinfo
    #######################################################################################################################################
    parser_mkchrinfo = prep_subs.add_parser('chrinfo', help='Optional. This can output a chrinfo file for you, from a genome file. ',
        epilog='example usage: "nimrod prep chrinfo --fa hg19.fa --out hg19.chrinfo" ' )
    
    parser_mkchrinfo.add_argument('--genome', help='path to a genome file (.fa or .pickle)', metavar='FILE', required=True)
    parser_mkchrinfo.add_argument('--out', help='output files', metavar='FILE', required=True)
            
    # this would have been nice to use, but I can't get it to import TF when I want it to, which
    # ends up importing TF during the command line help which sucks.
    # parser_mkchrinfo.set_defaults(func=mkgenomeindex)
    parser_mkchrinfo.add_argument('--myfunc',  default="mk_chrinfo", help=argparse.SUPPRESS) 




    # Make the bigwig files
    #######################################################################################################################################
    # construct bw files that offer very quick access to the atac signal per basepair.# Gets called internally if .bam files are given as atac input.
    parser_b = prep_subs.add_parser('bigwig', 
        help='DataPreparation 3/4: Besides a genome, nimrod also requires the ATAC signal from each condition.\
        This command constructs a series of big wig files and an index from your .bam files. You should run this once for each collection of bam files that \
        you intend to use with the model.',
        
        description="This command creates a folder with bigwig files configured the way nimrod expects them. Don't use your own bigwig files. \
        You will have to prepare a simple json file (it's just a txt file with specific structure) describing your stages and where your bam files are. \
        The --printjson argument prints a template file that you can edit to your needs. The keys/stage names of the json file must \
        match those in your dataset later.",

        epilog='example usage: "nimrod prep bigwig --procs 16 --json human_lines.json --chrinfo hg19.txt --out human_lines_nimrod_bw" ')


    parser_b.add_argument('--json', help='Path to a json seed file', metavar='',required=False)
    parser_b.add_argument('--printjson', help='Print an example of the .json file expected in the --json argument.', action='store_true', default=False, required=False)
    parser_b.add_argument('--out', help='Name of the output folder', metavar='',required=False)
    parser_b.add_argument('--chrinfo', type=str, default='notgiven',help="""
                       A two column |<chromosome name> <total_length>| txt file.
                       The chromosome names must match those on the bam file. nimrod only 
                       works on the chromosomes found in this file, so if you want to create a bigwig with only 
                       some chromosomes included, edit this input file accordingly.""", metavar='',required=False)
    parser_b.add_argument('--procs', type=int, default=4, help="""
                       The number of processors to use. Defaults to 4. Workload is balanced by chromosomes.""",metavar='')
    parser_b.add_argument('--verbose', action='store_true', default=False, help=argparse.SUPPRESS)
    
    # this would have been nice to use, but I can't get it to import TF when I want it to, which
    # ends up importing TF during the command line help which sucks.
    # parser_b.set_defaults(func=make_bws)
    parser_b.add_argument('--myfunc',  default="make_bws", help=argparse.SUPPRESS) 
    
    # preproc
    #######################################################################################################################################
    parser_pp = prep_subs.add_parser('ndata',
        # formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description='This subcommand prepares the input data for the model. You should do this for each set of motifs (each "dataset") that you will use to \
        train a model. The dataset is a bed-like tsv file with the following columns: [chrom    start   end stage   score   iT  strand  set \]. \
        where stage should be the stage (from the ones given in the bigwig file preparation) that each site should be used in. iT is the label column \
        and should be 1 for true motifs (overlapping chipseq peak?) and 0 for false motifs. set should be one of the following three: "train", "validation","test"\
        We have been using 1/10 of total sites for testing and another 1/10 for validation.',
        
        help='DataPreparation 4/4: Prepares data for nimrod based on the sites given by a datatable.', 
        epilog='example usage: "nimrod prep ndata --genome hg19.pickle --bwf human_lines_nimrod_bw --df CTCF_sites_human_lines.tsv --out nimrod_preproc_1"', 
        )

    parser_pp.add_argument('--genome', help='path to a genome file (.fa or .pickle)', metavar='FILE', required=False)
    parser_pp.add_argument('--bwf', help='path to a bw folder made with the "nimrod prep" command', metavar='FOLDER', required=False)
    parser_pp.add_argument('--df', help='path to a table with the motif hits formatted properly', metavar='FILE', required=False)
    parser_pp.add_argument('--out', help='path to folder where things will be output', metavar='FOLDER', required=True)
    parser_pp.add_argument('--win_size', type=int, default=1000, help=argparse.SUPPRESS)
    parser_pp.add_argument('--myfunc',  default="mkchunks_manager", help=argparse.SUPPRESS)


    # Train
    #######################################################################################################################################
    parser_a = subparsers.add_parser('train',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description='This subcommand initializes and trains a neural network.\
        Make sure that you have prepared your data (genome, preproc, motifs) \
        before you start this step.',
        
        help='Train a model. Make sure you have prepared the data and your motifs table before this step.', 
        epilog='example usage: "nimrod train --epochs 10 --dexe 1000 --bsm 10 --prep nimrod_preproc_1 --genome hg19.pickle --df CTCF_sites_human_lines.tsv --out nimrod_training_1"', 
        )

    parser_a.add_argument('--genome', help='path to a genome file (.fa or .pickle)', metavar='FILE', required=False)
    parser_a.add_argument('--df',     help='path to a table with the motif hits, stage, isReal, set', metavar='FILE', required=False)
    parser_a.add_argument('--ndata',   help="path to folder created with the 'nimrod prep ndata' command ",  metavar='FOLDER', default="NOPE")
    parser_a.add_argument('--out',    help='path to output folder', metavar='FOLDER', required=True)
    
    parser_a.add_argument('--epochs', help='maximum number of epochs to run', metavar='NUM',type=int , default=300)
    parser_a.add_argument('--valfre',  metavar='NUM',type=int, default=5000 , help="""Validation Freequency: Every how many batches should I test the validation set?""")
    parser_a.add_argument('--patience', metavar='NUM',type=int, default=10 , help="""If we don't improve after this many validation tests, stop the training """)

    parser_a.add_argument('--slr', type=float, default=0.00001, help="Starting Learning Rate.")
    parser_a.add_argument('--dexe', type=float, default=5, help="The learning rate decays to LR*0.9 every *dexe* epochs")
    parser_a.add_argument('--bsm', type=int, default=2, help="Batch size multiplier. Try values from 2-50+. Use the 'nimrod aux bsm_analysis for help.")

    
    # batch size multiplier: if we have 1positive:Xnegatives,
    # the minibatchsize during training will be bsm*(X+1)
    parser_a.add_argument('--model', default=None,metavar='FILE', help=argparse.SUPPRESS)
    parser_a.add_argument('--weights', metavar='NUM',type=float, nargs='+', default=[1., 1.] , help=argparse.SUPPRESS)
    parser_a.add_argument('--bwf', help=argparse.SUPPRESS, metavar='FOLDER', required=False)
    parser_a.add_argument('--test', action='store_true', default=False, help=argparse.SUPPRESS)
    parser_a.add_argument('--win_size', type=int, default=1000, help=argparse.SUPPRESS)

    
    # this would have been nice to use, but I can't get it to import TF when I want it to, which
    # ends up importing TF during the command line help which sucks.
    # parser_a.add_argument('--cache_chunks', choices=['000','001','010','100','011','110','101','111'], default='011', help=argparse.SUPPRESS)
    parser_a.add_argument('--myfunc',  default="training_manager", help=argparse.SUPPRESS) 


    # CONTINUE Train
    #######################################################################################################################################
    parser_cont_a = subparsers.add_parser('train_cont',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description='Take a trained model and continue training it with more data.',
        help='Continue training a model. ', 
        )

    parser_cont_a.add_argument('--genome', help='path to a genome file (.fa or .pickle)', metavar='FILE', required=False)
    parser_cont_a.add_argument('--df',     help='path to a table with the motif hits, stage, isReal, set', metavar='FILE', required=False)
    parser_cont_a.add_argument('--ndata',   help="path to folder created with the 'makechunks' command ",  metavar='FOLDER', default="NOPE")
    parser_cont_a.add_argument('--out',    help='path to output folder', metavar='FOLDER', required=True)
    
    parser_cont_a.add_argument('--epochs', help='maximum number of epochs to run', metavar='NUM',type=int , default=300)
    parser_cont_a.add_argument('--valfre',  metavar='NUM',type=int, default=5000 , help="""Validation Freequency: Every how many batches should I test the validation set?""")
    parser_cont_a.add_argument('--patience', metavar='NUM',type=int, default=10 , help="""If we don't improve after this many validation tests, stop the training """)

    parser_cont_a.add_argument('--slr', type=float, default=0.00001, help="Starting Learning Rate.")
    parser_cont_a.add_argument('--dexe', type=float, default=5, help="The learning rate decays to LR*0.9 every *dexe* epochs")
    parser_cont_a.add_argument('--bsm', type=int, default=2, help="batch size multiplier. Try values from 2-50+")
    parser_cont_a.add_argument('--modelP', help='path to a nimrod model made with the train command. \
        Something like: "/path/to/nimrod_results/model_states/model_state_loss_at-10000" ', metavar='pathname', required=True)

    
    # batch size multiplier: if we have 1positive:Xnegatives,
    # the minibatchsize during training will be bsm*(X+1)
    parser_cont_a.add_argument('--model', default=None,metavar='FILE', help=argparse.SUPPRESS)
    parser_cont_a.add_argument('--weights', metavar='NUM',type=float, nargs='+', default=[1., 1.] , help=argparse.SUPPRESS)
    parser_cont_a.add_argument('--bwf', help=argparse.SUPPRESS, metavar='FOLDER', required=False)
    parser_cont_a.add_argument('--test', action='store_true', default=False, help=argparse.SUPPRESS)
    parser_cont_a.add_argument('--win_size', type=int, default=1000, help=argparse.SUPPRESS)


    
    # this would have been nice to use, but I can't get it to import TF when I want it to, which
    # ends up importing TF during the command line help which sucks.
    # parser_cont_a.add_argument('--cache_chunks', choices=['000','001','010','100','011','110','101','111'], default='011', help=argparse.SUPPRESS)
    parser_cont_a.add_argument('--myfunc',  default="continue_training_manager", help=argparse.SUPPRESS) 



    # Test
    #######################################################################################################################################
    parser_test = subparsers.add_parser('test', help='Apply a trained model on a test dataset')
    parser_test.add_argument('--genome', help='path to a genome file (.fa or .pickle)', metavar='FILE', required=True)
    parser_test.add_argument('--df', help='path to a table with the motif hits, stage, isReal, set', metavar='FILE', required=True)
    parser_test.add_argument('--bwf', help='path to a bw folder made with the makebw command', metavar='FOLDER', required=False)      
    parser_test.add_argument('--modelP', help='path to a nimrod model made with the train command. \
        Something like: "/path/to/nimrod_results/model_states/model_state_loss_at-10000" ', metavar='pathname', required=True)
    
    # parser_test.add_argument('--ndata',   help="path to folder created with the 'makechunks' command ",  metavar='FOLDER', default="NOPE")
    parser_test.add_argument('--out',    help='path to output folder', metavar='FOLDER', required=True)
    
    # if set, will run test on all motifs, not just the 'test' ones
    # parser_test.add_argument('--testemall', action='store_true', default=False, help=argparse.SUPPRESS)
    # parser_cont_a.add_argument('--epochs', help='maximum number of epochs to run', metavar='NUM',type=int , default=300)
    # parser_cont_a.add_argument('--valfre',  metavar='NUM',type=int, default=5000 , help="""Validation Freequency: Every how many batches should I test the validation set?""")
    # parser_cont_a.add_argument('--patience', metavar='NUM',type=int, default=10 , help="""If we don't improve after this many validation tests, stop the training """)

    # parser_cont_a.add_argument('--slr', type=float, default=0.00001, help="Starting Learning Rate.")
    # parser_cont_a.add_argument('--dexe', type=float, default=5, help="The learning rate decays to LR*0.9 every *dexe* epochs")
    # parser_cont_a.add_argument('--bsm', type=int, default=2, help="batch size multiplier. Try values from 2-50+")
    
    parser_test.add_argument('--weights', metavar='NUM',type=float, nargs='+', default=[1., 1.] , help=argparse.SUPPRESS)
    parser_test.add_argument('--win_size', type=int, default=1000, help=argparse.SUPPRESS)
    parser_test.add_argument('--cache_chunks', choices=['000','001','010','100','01','110','101','111'], default='010', help=argparse.SUPPRESS)
    parser_test.add_argument('--test', action='store_true', default=True, help=argparse.SUPPRESS)

    
    # this would have been nice to use, but I can't get it to import TF when I want it to, which
    # ends up importing TF during the command line help which sucks.
    # parser_test.set_defaults(func=prepare_test_fromargs)
    parser_test.add_argument('--myfunc',  default="prepare_test_fromargs", help=argparse.SUPPRESS) 
    
    # if len(sys.argv)<3 :
    #     parser.print_help(sys.stderr)
    #     sys.exit()


    
    
    args = parser.parse_args()

    args.not_float_printables = ['time','epoch','ustep','willSaveModelState','lrnRate']
    args.float_printables = ['loss', 'auc','aps','F05','F1','F2','Precision','youden', 'sensitivity', 'specificity', 'fpr']
    return args



