# import datetime
# import itertools
# import sys,os
# from multiprocessing import Process, Queue, Pool, Manager

# import pickle

# from scipy.ndimage.filters import gaussian_filter1d as scGF

import tensorflow as tf

def weight_variable(shape, myname):
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial, name=myname )

def bias_variable(shape, myname):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial, name=myname )

class NimrodNN():

	def __init__(self, WIDTH=1000, EXSTART=0.9, SLR=0.00001, EXDSTEPS=100000 ):

		self.seqInpCHannels = 5
		self.classNUM = 2
		self.inpLen = WIDTH
		# Placeholders, these need to be fed with the feed-dict
		self.atac_inp    	= tf.placeholder(tf.float32, shape=[None, self.inpLen,3, 2],             			name = "atac_inp")
		self.seq_inp     	= tf.placeholder(tf.float32, shape=[None, self.inpLen, self.seqInpCHannels],   		name = "seq_inp")
		self.labels_inp  	= tf.placeholder(tf.float32, shape=[None, self.classNUM],                			name = "labels_inp")
		self.FC2_keep  		= tf.placeholder(tf.float32,                                        				name = "FC2_keep")
		self.class_weights 	= tf.placeholder(tf.float32,  shape=[1,2],                        					name='class_weights') #[[ 1., 1. ]]
		#######################################################################################################################################
		### ATAC LAYER 1
		#######################################################################################################################################
		self.atacL1_ws       = 7
		self.atacL1_chN      = 64
		self.L_atacL1        = self.inpLen - self.atacL1_ws + 1
		####
		self.W_atacL1        = weight_variable([ self.atacL1_ws,3 , 2, self.atacL1_chN],               			myname='W_atacL1')
		self.b_atacL1        = bias_variable([self.atacL1_chN],                                   				myname='b_atacL1')
		self.h_atacL1        = tf.nn.relu( tf.nn.conv2d(self.atac_inp, self.W_atacL1, strides=[1,1,1,1], padding='VALID') + self.b_atacL1,
		                                                                                						name='h_atacL1')
		self.h_atacL1        = tf.reshape(self.h_atacL1, (-1,self.L_atacL1,self.atacL1_chN ) )

		#### atac layer 2
		#######################################################################################################################################
		self.atacL2_ws       = 25
		self.atacL2_chN      = 128
		self.L_atacL2        = self.L_atacL1 - self.atacL2_ws + 1
		####
		self.W_atacL2        = weight_variable([ self.atacL2_ws, self.atacL1_chN, self.atacL2_chN],         myname='W_atacL2')
		self.b_atacL2        = bias_variable([self.atacL2_chN],                                   			myname='b_atacL2')
		self.h_atacL2        = tf.nn.relu( tf.nn.conv1d(self.h_atacL1, self.W_atacL2, stride=1, padding='VALID')  + self.b_atacL2, 
		                                                                                					name='h_atacL2')
		#### Seq layer 1
		#######################################################################################################################################
		self.seqL1_ws        = 7
		self.seqL1_chN       = 128
		self.L_seqL1         = self.inpLen - self.seqL1_ws + 1
		self.W_seqL1         = weight_variable([ self.seqL1_ws, self.seqInpCHannels, self.seqL1_chN],       myname='W_seqL1')
		self.b_seqL1         = bias_variable([self.seqL1_chN],                                    			myname='b_seqL1')
		self.h_seqL1         = tf.nn.relu( tf.nn.conv1d(self.seq_inp, self.W_seqL1, stride=1, padding='VALID')  + self.b_seqL1, 
		                                                                                					name='h_seqL1')
		#### Seq layer 2
		#######################################################################################################################################
		self.seqL2_ws        = 25
		self.seqL2_chN       = 128
		self.L_seqL2         = self.L_seqL1 - self.seqL2_ws + 1
		self.W_seqL2         = weight_variable([ self.seqL2_ws, self.seqL1_chN, self.seqL2_chN],            myname='W_seqL2')
		self.b_seqL2         = bias_variable([self.seqL2_chN],                                    			myname='b_seqL2')
		self.h_seqL2         = tf.nn.relu( tf.nn.conv1d(self.h_seqL1, self.W_seqL2, stride=1, padding='VALID')  + self.b_seqL2, 
		                                                                                					name='h_seqL2')
		# The zipper:
		#######################################################################################################################################
		# 252 neurons from ATAC and 252 from Sequence, get aligned and concatenated
		# which creates 252 neurons, reading 128+128 channels each
		# and outputing comboFc1_chN channels each
		#######################################################################################################################################
		self.mezz_width = self.inpLen - self.seqL1_ws +1 - self.seqL2_ws +1
		self.mezzanine  = tf.stack([self.h_atacL2,self.h_seqL2],axis=2,name='mezzanine')
		self.mezzanine 	= tf.reshape(self.mezzanine, [tf.shape(self.h_seqL2)[0], self.mezz_width,256], 		name="mezzanine")

		# Combo Layer 1
		#######################################################################################################################################
		self.L_comboFc1      = self.mezz_width
		self.comboFc1_ws     = 1
		self.comboFc1_depth  = self.atacL2_chN + self.seqL2_chN
		self.comboFc1_chN    = 256
		self.W_comboFc1      = weight_variable([ self.comboFc1_ws,self.comboFc1_depth,self.comboFc1_chN ],    myname='W_comboFc1')
		self.b_comboL1       = bias_variable([self.comboFc1_chN], myname='b_comboFC1')
		self.h_comboFc1      = tf.nn.sigmoid(tf.nn.conv1d(self.mezzanine, self.W_comboFc1,stride=1, padding='VALID') + self.b_comboL1,      
			name='h_comboFc1') 
		# Maxpool 1
		#######################################################################################################################################
		self.maxpoolwindowsize = 5
		self.h_comboFc1_maxpooled = tf.layers.max_pooling1d( self.h_comboFc1, self.maxpoolwindowsize, self.maxpoolwindowsize, padding='VALID' )
		self.L_comboFc1_maxpooled = int(self.L_comboFc1 / self.maxpoolwindowsize)
		self.L_comboL1      = self.L_comboFc1_maxpooled - self.comboFc1_ws  + 1
		self.comboL1_ws     = 15
		self.comboL1_chN    = 256
		# Combo Layer 1
		#######################################################################################################################################
		self.W_comboL1      = weight_variable([ self.comboL1_ws,self.comboFc1_chN,self.comboL1_chN ],    myname='W_comboL1')
		self.b_comboL1      = bias_variable([self.comboL1_chN], myname='b_comboL1')
		self.h_comboL1      = tf.nn.relu(tf.nn.conv1d(self.h_comboFc1_maxpooled, self.W_comboL1,stride=1, padding='VALID') + self.b_comboL1,      
			name='h_comboL1') 
		# FULLY CONNECTED 2
		#######################################################################################################################################
		self.L_FC2 = self.L_comboL1 - self.comboL1_ws + 1
		self.S_FC2       = self.L_FC2 * self.comboL1_chN
		self.FC2_chN = 256
		self.W_FC2       = weight_variable([ self.S_FC2, self.FC2_chN],                    myname='W_FC2')
		self.b_FC2       = bias_variable([self.FC2_chN],                                  myname='b_FC2')
		self.h_FC2_flat  = tf.reshape(self.h_comboL1, [-1, self.S_FC2])
		self.h_FC2       = tf.nn.sigmoid(tf.matmul(self.h_FC2_flat, self.W_FC2) + self.b_FC2,  name='h_FC2')
		self.h_FC2_drop  = tf.nn.dropout(self.h_FC2, self.FC2_keep)
		# READOUT
		#######################################################################################################################################
		self.W_FCR           = weight_variable([self.FC2_chN, self.classNUM],                     myname='W_FCR')
		self.b_FCR           = bias_variable([self.classNUM],                                     myname='b_FCR')
		self.y_conv          = tf.matmul(self.h_FC2_drop, self.W_FCR, name='y_conv') + self.b_FCR

		# outputs
		#######################################################################################################################################
		self.softmax_batch_weights   = tf.reduce_sum( self.labels_inp * self.class_weights, 1,     name='softmax_batch_weights')

		# BETTER SCORE
		self.sm_score           = tf.nn.softmax( self.y_conv, name="score")

		self.cross_entropy   = tf.losses.softmax_cross_entropy(
		                                    onehot_labels = self.labels_inp,
		                                    logits=self.y_conv,
		                                    weights=self.softmax_batch_weights,
		                                    reduction=tf.losses.Reduction.MEAN)

		self.starter_learning_rate = SLR
		
		self.global_step = tf.Variable(0, trainable=False)
		self.learning_rate = tf.train.exponential_decay(self.starter_learning_rate, self.global_step, EXDSTEPS, EXSTART, staircase=False, name='learning_rate')
		self.train_step      = tf.train.AdamOptimizer(self.learning_rate).minimize(self.cross_entropy, global_step=self.global_step, name='train_step')




class NimrodNN_manualLR():

	def __init__(self, WIDTH=1000, EXSTART=0.9, EXDSTEPS=100000 ):

		self.seqInpCHannels = 5
		self.classNUM = 2
		self.inpLen = WIDTH
		# Placeholders, these need to be fed with the feed-dict
		self.atac_inp    	= tf.placeholder(tf.float32, shape=[None, self.inpLen,3, 2],             			name = "atac_inp")
		self.seq_inp     	= tf.placeholder(tf.float32, shape=[None, self.inpLen, self.seqInpCHannels],   		name = "seq_inp")
		self.labels_inp  	= tf.placeholder(tf.float32, shape=[None, self.classNUM],                			name = "labels_inp")
		self.FC2_keep  		= tf.placeholder(tf.float32,                                        				name = "FC2_keep")
		self.class_weights 	= tf.placeholder(tf.float32,  shape=[1,2],                        					name='class_weights') #[[ 1., 1. ]]
		#######################################################################################################################################
		### ATAC LAYER 1
		#######################################################################################################################################
		self.atacL1_ws       = 7
		self.atacL1_chN      = 64
		self.L_atacL1        = self.inpLen - self.atacL1_ws + 1
		####
		self.W_atacL1        = weight_variable([ self.atacL1_ws,3 , 2, self.atacL1_chN],               			myname='W_atacL1')
		self.b_atacL1        = bias_variable([self.atacL1_chN],                                   				myname='b_atacL1')
		self.h_atacL1        = tf.nn.relu( tf.nn.conv2d(self.atac_inp, self.W_atacL1, strides=[1,1,1,1], padding='VALID') + self.b_atacL1,
		                                                                                						name='h_atacL1')
		self.h_atacL1        = tf.reshape(self.h_atacL1, (-1,self.L_atacL1,self.atacL1_chN ) )

		#### atac layer 2
		#######################################################################################################################################
		self.atacL2_ws       = 25
		self.atacL2_chN      = 128
		self.L_atacL2        = self.L_atacL1 - self.atacL2_ws + 1
		####
		self.W_atacL2        = weight_variable([ self.atacL2_ws, self.atacL1_chN, self.atacL2_chN],         myname='W_atacL2')
		self.b_atacL2        = bias_variable([self.atacL2_chN],                                   			myname='b_atacL2')
		self.h_atacL2        = tf.nn.relu( tf.nn.conv1d(self.h_atacL1, self.W_atacL2, stride=1, padding='VALID')  + self.b_atacL2, 
		                                                                                					name='h_atacL2')
		#### Seq layer 1
		#######################################################################################################################################
		self.seqL1_ws        = 7
		self.seqL1_chN       = 128
		self.L_seqL1         = self.inpLen - self.seqL1_ws + 1
		self.W_seqL1         = weight_variable([ self.seqL1_ws, self.seqInpCHannels, self.seqL1_chN],       myname='W_seqL1')
		self.b_seqL1         = bias_variable([self.seqL1_chN],                                    			myname='b_seqL1')
		self.h_seqL1         = tf.nn.relu( tf.nn.conv1d(self.seq_inp, self.W_seqL1, stride=1, padding='VALID')  + self.b_seqL1, 
		                                                                                					name='h_seqL1')
		#### Seq layer 2
		#######################################################################################################################################
		self.seqL2_ws        = 25
		self.seqL2_chN       = 128
		self.L_seqL2         = self.L_seqL1 - self.seqL2_ws + 1
		self.W_seqL2         = weight_variable([ self.seqL2_ws, self.seqL1_chN, self.seqL2_chN],            myname='W_seqL2')
		self.b_seqL2         = bias_variable([self.seqL2_chN],                                    			myname='b_seqL2')
		self.h_seqL2         = tf.nn.relu( tf.nn.conv1d(self.h_seqL1, self.W_seqL2, stride=1, padding='VALID')  + self.b_seqL2, 
		                                                                                					name='h_seqL2')
		# The zipper:
		#######################################################################################################################################
		# 252 neurons from ATAC and 252 from Sequence, get aligned and concatenated
		# which creates 252 neurons, reading 128+128 channels each
		# and outputing comboFc1_chN channels each
		#######################################################################################################################################
		self.mezz_width = self.inpLen - self.seqL1_ws +1 - self.seqL2_ws +1
		self.mezzanine  = tf.stack([self.h_atacL2,self.h_seqL2],axis=2,name='mezzanine')
		self.mezzanine 	= tf.reshape(self.mezzanine, [tf.shape(self.h_seqL2)[0], self.mezz_width,256], 		name="mezzanine")

		# Combo Layer 1
		#######################################################################################################################################
		self.L_comboFc1      = self.mezz_width
		self.comboFc1_ws     = 1
		self.comboFc1_depth  = self.atacL2_chN + self.seqL2_chN
		self.comboFc1_chN    = 256
		self.W_comboFc1      = weight_variable([ self.comboFc1_ws,self.comboFc1_depth,self.comboFc1_chN ],    myname='W_comboFc1')
		self.b_comboL1       = bias_variable([self.comboFc1_chN], myname='b_comboFC1')
		self.h_comboFc1      = tf.nn.sigmoid(tf.nn.conv1d(self.mezzanine, self.W_comboFc1,stride=1, padding='VALID') + self.b_comboL1,      
			name='h_comboFc1') 
		# Maxpool 1
		#######################################################################################################################################
		self.maxpoolwindowsize = 5
		self.h_comboFc1_maxpooled = tf.layers.max_pooling1d( self.h_comboFc1, self.maxpoolwindowsize, self.maxpoolwindowsize, padding='VALID' )
		self.L_comboFc1_maxpooled = int(self.L_comboFc1 / self.maxpoolwindowsize)
		self.L_comboL1      = self.L_comboFc1_maxpooled - self.comboFc1_ws  + 1
		self.comboL1_ws     = 15
		self.comboL1_chN    = 256
		# Combo Layer 1
		#######################################################################################################################################
		self.W_comboL1      = weight_variable([ self.comboL1_ws,self.comboFc1_chN,self.comboL1_chN ],    myname='W_comboL1')
		self.b_comboL1      = bias_variable([self.comboL1_chN], myname='b_comboL1')
		self.h_comboL1      = tf.nn.relu(tf.nn.conv1d(self.h_comboFc1_maxpooled, self.W_comboL1,stride=1, padding='VALID') + self.b_comboL1,      
			name='h_comboL1') 
		# FULLY CONNECTED 2
		#######################################################################################################################################
		self.L_FC2 = self.L_comboL1 - self.comboL1_ws + 1
		self.S_FC2       = self.L_FC2 * self.comboL1_chN
		self.FC2_chN = 256
		self.W_FC2       = weight_variable([ self.S_FC2, self.FC2_chN],                    myname='W_FC2')
		self.b_FC2       = bias_variable([self.FC2_chN],                                  myname='b_FC2')
		self.h_FC2_flat  = tf.reshape(self.h_comboL1, [-1, self.S_FC2])
		self.h_FC2       = tf.nn.sigmoid(tf.matmul(self.h_FC2_flat, self.W_FC2) + self.b_FC2,  name='h_FC2')
		self.h_FC2_drop  = tf.nn.dropout(self.h_FC2, self.FC2_keep)
		# READOUT
		#######################################################################################################################################
		self.W_FCR           = weight_variable([self.FC2_chN, self.classNUM],                     myname='W_FCR')
		self.b_FCR           = bias_variable([self.classNUM],                                     myname='b_FCR')
		self.y_conv          = tf.matmul(self.h_FC2_drop, self.W_FCR, name='y_conv') + self.b_FCR

		# outputs
		#######################################################################################################################################
		self.softmax_batch_weights   = tf.reduce_sum( self.labels_inp * self.class_weights, 1,     name='softmax_batch_weights')

		# BETTER SCORE
		self.sm_score           = tf.nn.softmax( self.y_conv, name="score")

		self.cross_entropy   = tf.losses.softmax_cross_entropy(
		                                    onehot_labels = self.labels_inp,
		                                    logits=self.y_conv,
		                                    weights=self.softmax_batch_weights,
		                                    reduction=tf.losses.Reduction.MEAN)

		
		self.global_step = tf.Variable(0, trainable=False)
		
		# self.starter_learning_rate = SLR
		# self.learning_rate = tf.train.exponential_decay(self.starter_learning_rate, self.global_step, EXDSTEPS, EXSTART, staircase=False, name='learning_rate')
		
		self.learning_rate = tf.placeholder(tf.float32, name='learning_rate')

		self.train_step      = tf.train.AdamOptimizer(self.learning_rate).minimize(self.cross_entropy, global_step=self.global_step, name='train_step')
