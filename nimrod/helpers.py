


import pandas as pd
import numpy as np
import json
import itertools
import pyBigWig
from os import path,makedirs
# from pybedtools import BedTool as BT
import pybedtools as pBT

import shutil
from multiprocessing import Pool
import SharedArray as sha
import os,sys

from sklearn.metrics import roc_auc_score as ROCAUCSCORE
from sklearn.metrics import average_precision_score as APScore
from sklearn.metrics import brier_score_loss
import pickle




def load_genome(theargs):
    # if the input genome is a fasta, load the dictionary
    if theargs.genome.split(".")[-1] in ["fa","fasta"]:
        theargs.fa = theargs.genome
        genomeD = fa_toD(theargs)
    # else load the pickle
    elif theargs.genome.split(".")[-1] in ["pickle"]:
        genomeD = pickle.load( open(theargs.genome, 'rb') )
    return genomeD



def lite_load_datadf(args):
    df = pd.read_csv(args.df, sep='\t')
    for thing in ['chrom','start','end','stage','score','iT','strand','set']:
        assert thing in df.columns, "Could not find a column named {} in the input data".format(thing)
    extendby=int(args.win_size//2)
    df.loc[df.strand=='-','end'] = df.loc[df.strand=='-','end'] + extendby
    df.loc[df.strand=='+','start'] = df.loc[df.strand=='+','start'] - extendby
    df.loc[df.strand=='-','start'] = df.loc[df.strand=='-','end'] - args.win_size
    df.loc[df.strand=='+','end'] = df.loc[df.strand=='+','start'] + args.win_size
    df['start'] = df['start'].clip(lower=0)
    df['end'] = df['end'].clip(upper=df.chrom.map(args.chromsizeD))
    df = df[(df.end-df.start)==args.win_size]
    df = df.reset_index(drop=True)

    return df









def make_bw_dict(THEFP):
    bwD = json.load(open("{}/index.json".format(THEFP), "r"))
    fullp = lambda fp: os.path.join(THEFP,fp)
    prloadD = {}
    for stage, stagelist in bwD.items():
        
        prloadD[stage] = []
        for repl in stagelist:
            prloadD[stage].append(
                    {
                'FrC1' : { 'n' : pyBigWig.open( fullp(repl['FrC1']['n']) ),
                           'p' : pyBigWig.open( fullp(repl['FrC1']['p']) )},
                'FrC2' : { 'n' : pyBigWig.open( fullp(repl['FrC2']['n']) ),
                           'p' : pyBigWig.open( fullp(repl['FrC2']['p']) )},
                'FrC3' : { 'n' : pyBigWig.open( fullp(repl['FrC3']['n']) ),
                           'p' : pyBigWig.open( fullp(repl['FrC3']['p']) )}}
                )
    return prloadD

    



def fa_toD(args):
    seq = {}
    currchrom = None
    with open(args.fa) as fi:
        for line in fi:
            if line.startswith(">"):
                if currchrom:
                    seq[currchrom] = ''.join(seq[currchrom])
                currchrom = line.rstrip()[1:]
                seq[currchrom] = []
            else:
                seq[currchrom].append(line.rstrip())
        seq[currchrom] = ''.join(seq[currchrom])
    return seq





def make_dir_safely(savefolder):
    if path.exists(savefolder):
        print('\n\n\n WARNING: FOLDER "{}" ALREADY EXISTS \n\n\n'.format(savefolder))   
        name = input("Type 'okay' to continue anyway. The folder will be removed and remade.")
        if not name == 'okay':
            sys.exit("Not okay, closing down")
        else:
            shutil.rmtree(savefolder)
    makedirs(savefolder)


def do_the_output_folder(theargs):
    savefolder = path.abspath(theargs.out)    
    make_dir_safely(savefolder)
    makedirs( path.join(savefolder, "model_states" ))
    makedirs( path.join(savefolder, "test_results"))
    return savefolder









def load_premade_df(folder, args):
            # df_train, df_valid, df_test = load_datadf(theargs)

    dffp = path.join(folder, "motif_dataframe.tsv") 
    df = pd.read_csv( dffp, sep='\t', index_col=0)

    if 'test' in df['set'].unique():
        test_df = df[df['set'] == 'test'].copy()
        test_df.to_csv("{}/df_test.tsv".format(args.savefolder),sep='\t',index=False)

    else:
        test_df = None
    
    validation_df = df[df['set'] == 'validation'].copy()
    validation_df.to_csv("{}/df_valid.tsv".format(args.savefolder),sep='\t',index=False)

    train_df = df[df['set'] == 'train'].copy()
    train_df.to_csv("{}/df_train.tsv".format(args.savefolder),sep='\t',index=False)
    
    df.to_csv(args.savefolder+"/full_motif_dataframe.tsv", sep='\t', index=True)

    del df
    return train_df, validation_df, test_df


def load_datadf(args, case="train"):
    # any tests?
    df = pd.read_csv(args.df, sep='\t')
    for thing in ['chrom','start','end','stage','score','iT','strand','set']:
        assert thing in df.columns, "Could not find a column named {} in the input data".format(thing)
    
    if case=="train":        
        assert "train" in df['set'].unique()
        assert "validation" in df['set'].unique()
    
    if args.test:
        assert "test" in df['set'].unique()

    extendby=int(args.win_size//2)
    df.loc[df.strand=='-','end'] = df.loc[df.strand=='-','end'] + extendby
    df.loc[df.strand=='+','start'] = df.loc[df.strand=='+','start'] - extendby
    df.loc[df.strand=='-','start'] = df.loc[df.strand=='-','end'] - args.win_size
    df.loc[df.strand=='+','end'] = df.loc[df.strand=='+','start'] + args.win_size
    df['start'] = df['start'].clip(lower=0)
    df['end'] = df['end'].clip(upper=df.chrom.map(args.chromsizeD))
    df = df[(df.end-df.start)==args.win_size]
    df = df.reset_index(drop=True)

    if 'test' in df['set'].unique():
        test_df = df[df['set'] == 'test'].copy()
    else:
        test_df = None

    if case=='train':
        validation_df = df[df['set'] == 'validation'].copy()
        validation_df.to_csv("{}/df_valid.tsv".format(args.savefolder),sep='\t',index=False)

        train_df = df[df['set'] == 'train'].copy()
        train_df = train_df.iloc[np.random.permutation(len(train_df)) ].reset_index(drop=True)
        train_df.to_csv("{}/df_train.tsv".format(args.savefolder),sep='\t',index=False)

    elif case=='test':
        validation_df = None
        train_df = None
        test_df = df[df['set'] == 'test'].copy()
        test_df.to_csv("{}/df_test.tsv".format(args.savefolder),sep='\t',index=False)
    else:
        validation_df = df[df['set'] == 'validation'].copy()
        validation_df.to_csv("{}/df_valid.tsv".format(args.savefolder),sep='\t',index=False)
        train_df = df[df['set'] == 'train'].copy()
        train_df.to_csv("{}/df_train.tsv".format(args.savefolder),sep='\t',index=False)
        
    df.to_csv(args.savefolder+"/motif_dataframe.tsv", sep='\t', index=True)

    return train_df, validation_df, test_df

def confusion_line_to_matrix(asstr):
    return np.array([x.split(",") for x in asstr.split(":")]).astype(float)
def confusion_matrix_to_line(m):
    return ":".join([",".join(x) for x in m.astype(str)])
def metric_Sensitivity(a):
    '''of the things that are True, what % did I call True'''
    return a[1,1]/(a[1,:].sum())
def metric_Recall(a):
    '''of the things that are True, what % did I call True'''
    return metric_Sensitivity(a)
def metric_Specificity(a):
    '''of the things that are False, what % did I call False'''
    return a[0,0]/(a[0,0]+a[0,1])
def metric_FPR(a):
    '''of the things that are False, what % did I call True'''
    return a[0,1]/(a[0,0]+a[0,1])
def metric_youden(a):
    return metric_Sensitivity(a) + metric_Specificity(a) -1
def metric_Precision(a):
    '''of the things i called true, what % is indeed True'''
    return a[1,1]/a[:,1].sum()
def metric_Fscore(a, b):
    p = metric_Precision(a)
    r = metric_Recall(a)
    f = (1 + b*b) * (p * r)/( b*b*p +r )
    return f



def complement( s):
    compD = {'A' :'T','C':'G','G':'C','T':'A','a':'t','c':'g','g':'c','t':'a','N':'N','n':'n'}
    return ''.join([compD[x] for x in s])
def reverse( s):
    return s[::-1]

def assert_bw_dict(THEFP):
    bwD = json.load(open("{}/index.json".format(THEFP), "r"))
    fullp = lambda fp: os.path.join(THEFP,fp)
    for stage, stagelist in bwD.items():
        for repl in stagelist:
            assert path.isfile( fullp(repl['FrC1']['n'])), f"not found: {fullp(repl['FrC1']['n'])}"
            assert path.isfile( fullp(repl['FrC1']['p'])), f"not found: {fullp(repl['FrC1']['p'])}"
            assert path.isfile( fullp(repl['FrC2']['n'])), f"not found: {fullp(repl['FrC2']['n'])}"
            assert path.isfile( fullp(repl['FrC2']['p'])), f"not found: {fullp(repl['FrC2']['p'])}"
            assert path.isfile( fullp(repl['FrC3']['n'])), f"not found: {fullp(repl['FrC3']['n'])}"
            assert path.isfile( fullp(repl['FrC3']['p'])), f"not found: {fullp(repl['FrC3']['p'])}"

    return 1





    



def chooser(lista, chance_to_drop=0.666):	
    loi = []
    while len(loi) == 0:
        for thing in lista:
            if np.random.random() > chance_to_drop:
                loi.append(thing)
    return lista








# def unfold_batch(batch):
#     _peakui, _labels, _atacwi, _seqwi = list(zip(*batch))
#     batch_atacs = np.array(_atacwi)
#     batch_seqs = np.array(_seqwi)
#     batch_labels = np.array(_labels)
#     return _peakui, batch_labels, batch_atacs, batch_seqs


def get_the_model(theargs,alt=False):
    if theargs.model:
        try:
            import importlib.machinery
            modulename = importlib.machinery.SourceFileLoader('modulename',theargs.model).load_module()
            modelfilepath = path.abspath(modulename.__file__)
            themodel = modulename.NimrodNN(SLR=theargs.slr, EXDSTEPS=theargs.eds)
        except:
            sys.exit("Failed to load the user-specified model")
    else:
        if not alt:
            from .model import NimrodNN
            themodel = NimrodNN(SLR=theargs.slr, EXDSTEPS=theargs.eds)
            dirname, filename = path.split(path.abspath(__file__))
            modelfilepath = path.join(dirname, "model.py")
        else:
            from .model import NimrodNN_manualLR
            themodel = NimrodNN_manualLR()
            dirname, filename = path.split(path.abspath(__file__))
            modelfilepath = path.join(dirname, "model.py")

    
    shutil.copy(modelfilepath, path.join(theargs.savefolder, "the_model.py") )

    return themodel


def print_train_report(theargs, cM):

    theargs.dfPositives = cM.dfPositives
    theargs.dfNegatives = cM.dfNegatives

    theargs.minibatch_negativesNum = int(theargs.dfNegatives / theargs.dfPositives)
    
    theargs.minibatch_positivesNum = cM.minibatch_positivesNum
    theargs.minibatch_negativesNum = cM.minibatch_negativesNum

    theargs.minibatch_base_num = cM.chunk_size
    theargs.batches_in_an_epoch = cM.how_many_batches_does_that_make
    theargs.eds = int( theargs.dexe * theargs.batches_in_an_epoch )


    with open(path.join(theargs.savefolder, "arguments_overview.txt"), 'w') as fo:
        aD = theargs.__dict__

        print("\n# Inputs", file=fo)
        
        if ((theargs.ndata == "JUSTSAVE") or (theargs.ndata == "NOPE")):
            print("The genomic sequences were loaded from\t{}".format(   path.abspath(aD['genome'])), file=fo)
            print("The ATAC signals were loaded from\t{}".format(        path.abspath(aD['bwf'])), file=fo)
            print("The motifs were loaded from\t{}".format(              path.abspath(aD['df'])), file=fo)


        print("\n# Other options:", file=fo)
        print("The maximum number of epochs (one epoch is one full pass of all the motifs) was set at: {}".format(   aD['epochs']), file=fo)

        if None is aD.get('model',None):
            print("The default model will be used", file=fo)
        else:
            print("A custom model will be used, loaded from :{}".format( aD['model']), file=fo)

        if not aD['test']:
            print("No test will be run (after training)", file=fo)
        else:
            print("A test will be run, according to the motif dataframe", file=fo)

        print("A validation-test (calculate loss,AUC etc over the validation data) will run every {} batches".format( aD['valfre']), file=fo)
        print("Early stop if for {} consecutive validation-tests no improvement is found on the loss".format( aD['patience']), file=fo)



        print("\n# Data details", file=fo)
        print("Total number of positive motifs:\t{}".format( aD['dfPositives']), file=fo)
        print("Total number of negative motifs:\t{}".format( aD['dfNegatives']), file=fo)

        print("\n# Model details", file=fo)
        print("## minibatch details", file=fo)
        print("Each minibatch will have {} negative motifs and one positive".format( aD['minibatch_negativesNum']), file=fo)
        print("The batch size multiplier was set to {}".format( aD['bsm']), file=fo)

        print("If negatives are more than positives:", file=fo)
        print(" each batch presented to the model will have (bsm + (#Neg/#pos)*bsm) =  {} + ({} / {})*{} = {} ".format( aD['bsm'] ,
                                                                                                                        aD['dfNegatives'],
                                                                                                                        aD['dfPositives'],
                                                                                                                        aD['bsm'],
                                                                                                                        int(aD['bsm'] + aD['bsm']*(aD['dfNegatives']/aD['dfPositives']) )
                                                                                                                         ), file=fo)

        print("Then each epoch will consist of {} batches".format(  aD['batches_in_an_epoch']), file=fo)

        print("\n## Learning Rate", file=fo)
        print("The starting learning rate was:\t{}".format(aD['slr']), file=fo)
        print("The learning rate was set to decay (new_rate = old_rate*0.9) every {} epochs".format( aD['dexe']), file=fo)
        print("The learning rate will decay exponentially every {} batches".format( aD['eds']), file=fo)
        print("The weights for the negative and positive classes were set at {} : {}".format(  *aD['weights']), file=fo)
