# from functools import partial
from multiprocessing import Pool
from os import path,makedirs


import json
import numpy as np
import os,sys
import pandas as pd
import pickle
import pybedtools as pBT

from .b2bw import bamtobw
from .b2bw import make_the_parser as b2bw_arg_parser
from .b2bw import parse_args as b2bw_parse_args

from .helpers import load_genome,lite_load_datadf


from .helpers import print_train_report,get_the_model,fa_toD
from .helpers import load_datadf,load_premade_df
from .helpers import do_the_output_folder,assert_bw_dict
from .chunklib import make_chunks_sub,activate_chunks,worker0,worker066
from .chunklib import ChunkManager,give_me_chunks,give_me_chunks_withchange,delete_chunks
from .nimrod import run_test,report_test,handle_folders_for_test



"""
This should contain all, and only those, main nimrod functions such as 'train','makebw' etc
"""
# from .helpers import mk_chrinfo

def moods2tsv(args):
    """
    Converts a moods csv file to a normal bed file
    """
    if path.isfile(args.out):
            name = input("Output file already exists, type 'okay' to continue and remake it")
            if not name == 'okay':
                sys.exit()
    csvfp = args.csv
    outfp = args.out
    tfname = args.tfname

    thecsv = pd.read_csv(csvfp,sep=',', header=None,comment='#')
    fseq = thecsv.iloc[0,5]

    tflen = len(fseq)
    thecsv['end'] = thecsv[2] + tflen
    thecsv['name'] = tfname
    thecsv[[0,2,'end','name',4,3]].to_csv(outfp, sep='\t', header=False, index=False)

def mk_chrinfo(args):
    """
    prints a chrinfo file based on a genome
    """
    if path.isfile(args.out):
            name = input("chrinfo file already exists, type 'okay' to continue and remake it")
            if not name == 'okay':
                sys.exit()
    g = load_genome(args)
    with open(args.out,"w") as fo:
        for k,v in g.items():
            print(k,len(v), sep='\t', file=fo)



# from .helpers import kowalski
def kowalski(args):
    """
    Does a little analysis to help you decide what bsm value to choose
    Just prints some numbers
    """
    args.win_size = 1000 #why am i hardcoding this?
    genomeD = load_genome(args)
    args.chromsizeD = { k: len(v) for k,v in genomeD.items()}

    df = lite_load_datadf(args)
    df = df[df['set'] == 'train'].copy()
    
    dfPositives = df.iT.sum()
    print("Kw: Positives: {}".format(dfPositives))

    dfNegatives = len(df) - dfPositives
    print("Kw: Negatives: {}".format(dfNegatives))

    n2p = float(dfNegatives / dfPositives)
    print("Kw: n2p: {}".format(n2p))

    # print("bsm\tPositives/batch\tNegatives/batch\t#\tbatches")
    print(f" bsm\tpositives_in_batch\tnegatives_in_batch\tratio\thow_many_batches_does_that_make")
    for bsm in range(1,100):
        try:
            if n2p <1:
                # less negatives than positives
                positives_in_each_batch = bsm
                negatives_in_each_batch = int(positives_in_each_batch * n2p)
            else:
                # more negatives than positives
                negatives_in_each_batch = bsm
                positives_in_each_batch = int(negatives_in_each_batch * (1/n2p))
            if negatives_in_each_batch==0 or positives_in_each_batch==0:
                continue
            ratio = abs((negatives_in_each_batch/positives_in_each_batch)-n2p)
            total_in_batch = positives_in_each_batch + negatives_in_each_batch
            how_many_batches_does_that_make = int(len(df)/total_in_batch)
            

            print(f" {bsm}\t{positives_in_each_batch}\t{negatives_in_each_batch}\t{ratio}\t{how_many_batches_does_that_make}")
        except:
            continue


# from .helpers import makedataset
def makedataset(args):
    """
    Prepares a tsv file with the putative sites per stage, with train/validate/test set defined
    and with IsTrue (iT) value set. For the latter, we need chip peaks
    """
    # initial things
    if args.printjson:
        print("""
{
"chip_peaks" : {'stage1'  : '/path/to/somewhere/chip_peaks_stage1.bed',
                  'stage2'  : '/path/to/somewhere/chip_peaks_stage2.bed',
                  'stage3'  : '/path/to/somewhere/chip_peaks_stage3.bed'},
                  
"atac_peaks" : {'stage1': "/path/to/somewhere/atac_peaks_stage1.bed",
                'stage2': "/path/to/somewhere/atac_peaks_stage2.bed",
                'stage3': "/path/to/somewhere/atac_peaks_stage3.bed"},

"genomic_pwm" : "/path/to/somewhere/afile3.bed"
}
""")
        sys.exit()



    # TODO
    # assert f_chip and f_atac are in acceptable range
    assert args.json, "You need to set the --json argument"
    assert args.out, "No output file specified"
    if args.tempdir:
        assert path.exists(args.tempdir), "If you specify a temp folder, it must exist"
    if path.isfile(args.out):
        print('\n\n\n WARNING: FILE "{}" ALREADY EXISTS \n\n\n'.format(args.out))   
        name = input("Type 'okay' to continue anyway. The file will be removed and remade.")
        if not name == 'okay':
            sys.exit("Not okay, closing down")
        else:
            pass

    # set tempdir
    pBT.helpers.set_tempdir(args.tempdir)

    # if json is set, try to load it:
    assert path.isfile(args.json), "Can't find the json file you specified"
    j = json.load(open(args.json,"r"))
    try:
        print("Loading peak files")
        pwm = pBT.BedTool(path.expanduser(j['genomic_pwm']))
        for k,v in j['atac_peaks'].items():
            j['atac_peaks'][k] = pBT.BedTool(path.expanduser(v))
        if args.nochip:
            pass
        else:
            for k,v in j['chip_peaks'].items():
                j['chip_peaks'][k] = pBT.BedTool(path.expanduser(v))
        print()
    except Exception as ex:
        print("Failed to load bedfiles: ")
        print(ex)

    # If we want to sort the bedfiles
    # Doesn't seem to be worth it to be honest
    _aresorted = False #flag passed to pybedtools
    if args.sortthem:
        print("Sorting peak files:")
        print("the PWM hits")
        pwm = pwm.sort()
        print("the ATAC peaks")
        for k,v in j['atac_peaks'].items():
            j['atac_peaks'][k] = v.sort()
        if args.nochip:
            pass
        else:
            print("the CHIP peaks")
            for k,v in j['chip_peaks'].items():
                j['chip_peaks'][k] = v.sort()

        _aresorted = True
        print()
    elif args.aresorted:
        _aresorted = True

    # Tests and assertions
    if not args.nochip:
        assert set(j['atac_peaks'].keys()) == set(j['chip_peaks'].keys()), "The stage names given for ATAC and CHIPSEQ are not the same"

    stages = set(j['atac_peaks'].keys())
    lot =[]
    for stage in stages:
        print("Processing ",stage)
        ldf = pwm.intersect(b=j['atac_peaks'][stage], u=True,f=args.f_atac, sorted=_aresorted)
        
        if args.nochip:
            ldf_df = ldf.to_dataframe()
            ldf_df['iT'] = 0
        else:
            ldf = ldf.intersect(b=j['chip_peaks'][stage], c=True, f=args.f_chip , sorted=_aresorted)
            ldf_df = ldf.to_dataframe()
        ldf_df.columns = ['chrom','start','end','name','score','strand','iT']
        ldf_df['iT'] = ldf_df['iT'].apply(lambda x: x>0).astype(int)
        ldf_df['stage'] = stage
        print("hits inside ATAC peaks at this stage: {}".format(len(ldf)))

        if not args.nochip:
            print("Out of those, {} overlapped a CHIPseq peak, for a percentage of {}".format( ldf_df.iT.sum(), ldf_df.iT.sum()*100./len(ldf_df) ))

        ldf_df['set'] = 'train'
        if (not args.setsglobal) and (not args.nochip):

            falseses = (ldf_df.iT==0).sum()
            truthses = (ldf_df.iT==1).sum()
            
            minset = min([falseses,truthses])
            maxset = max([falseses,truthses])
            ratio = int(maxset/minset)
            minsample = int(minset*0.1)
            maxsample = minsample * ratio
            print(f">>>> falseses:{falseses} truthses:{truthses} minset:{minset} maxset:{maxset} ratio:{ratio} minsample:{minsample} maxsample:{maxsample}")
            if falseses > truthses:
                ldf_df.loc[ldf_df.loc[ (ldf_df.iT == 1) & (ldf_df.set=='train'),:].sample(minsample).index,'set'] = 'test'
                ldf_df.loc[ldf_df.loc[ (ldf_df.iT == 1) & (ldf_df.set=='train'),:].sample(minsample).index,'set'] = 'validation'
                ldf_df.loc[ldf_df.loc[ (ldf_df.iT == 0) & (ldf_df.set=='train'),:].sample(maxsample).index,'set'] = 'test'
                ldf_df.loc[ldf_df.loc[ (ldf_df.iT == 0) & (ldf_df.set=='train'),:].sample(maxsample).index,'set'] = 'validation'
            else:
                ldf_df.loc[ldf_df.loc[ (ldf_df.iT == 0) & (ldf_df.set=='train'),:].sample(minsample).index,'set'] = 'test'
                ldf_df.loc[ldf_df.loc[ (ldf_df.iT == 0) & (ldf_df.set=='train'),:].sample(minsample).index,'set'] = 'validation'
                ldf_df.loc[ldf_df.loc[ (ldf_df.iT == 1) & (ldf_df.set=='train'),:].sample(maxsample).index,'set'] = 'test'
                ldf_df.loc[ldf_df.loc[ (ldf_df.iT == 1) & (ldf_df.set=='train'),:].sample(maxsample).index,'set'] = 'validation'
                


        
        print()

        lot.append(ldf_df[['chrom','start','end','stage','score','iT','strand','set']].copy())
    df = pd.concat(lot)

    if args.setsglobal and (not args.nochip):
        falseses,truthses = df.iT.value_counts()
        ratio = int(falseses/truthses)
        dfsample = int(truthses*0.1)
        ftsample = dfsample * ratio
        df.loc[df.loc[ (df.iT == 1) & (df.set=='train'),:].sample(dfsample).index,'set'] = 'test'
        df.loc[df.loc[ (df.iT == 1) & (df.set=='train'),:].sample(dfsample).index,'set'] = 'validation'
        df.loc[df.loc[ (df.iT == 0) & (df.set=='train'),:].sample(ftsample).index,'set'] = 'test'
        df.loc[df.loc[ (df.iT == 0) & (df.set=='train'),:].sample(ftsample).index,'set'] = 'validation'
    if args.nochip:
        df['set'] = 'test'

    # Report
    if not args.nochip:
        rep = df.groupby('set').apply(lambda g: g.groupby('stage').apply(lambda g: g.groupby('iT').count()['chrom']) )
        rep['%'] = rep[1]*100./(rep[0]+rep[1])
        rep.columns = ['Negative Sites', 'Positive Sites', '% positives']
        print(rep)
        print()
    else:
        rep = df.groupby('set').apply(lambda g: g.groupby('stage').count()['chrom']) 
        # rep['%'] = rep[1]*100./(rep[0]+rep[1])
        # rep.columns = ['Negative Sites', 'Positive Sites', '% positives']
        print(rep)
        print()
    # Quit elegantly
    print("Saving output")
    df.to_csv(args.out, index=False, sep='\t')
    print("Cleaning up temporary pybedtools files")
    pBT.helpers.cleanup()










# from .arg_parser import mkgenomeindex
def mkgenomeindex(args, save=True):
    """Makes the genome pickle file from .fa file"""
    seq = fa_toD(args)
    
    if save:
        if path.isfile(args.out):
            name = input("genome index already exists, type 'okay' to continue and remake it")
            if not name == 'okay':
                sys.exit()
        pickle.dump(seq, open(args.out, 'wb') )
    return seq

# from .arg_parser import make_bws
def make_bws(args):
    """
    We canibalized code from https://github.com/panosfirmpas/bamToBigWig
    With this command, we make some terminal calls to the local ./bamToBigWig script
    Internalizing the module would have been more elegant, but slower
    This takes bam files as input and makes a folder with the necessary bigwig files
    """  
    if args.printjson:
        print("Notice that even if you only have one bam in a stage, it still needs to be in a list []")
        print("By putting multiple bam files in a list, we merge their signal")
        print("""{
"stage1": ["/path/to/stage1.bam"],

"stage2":    ["/path/to/stage2.bam",
                        "/path/to/stage2a.bam",
                        "/path/to/stage2b.bam"],

"stage3":    ["/path/to/stage3a.bam",
                        "/path/to/stage3b.bam",
                        "/path/to/stage3c.bam"]
}""")
        sys.exit()
    else:
        assert args.json, "You need to set the --json, --out  and --chrinfo arguments"
        assert args.out, "You need to set the --json, --out  and --chrinfo arguments"
        assert args.chrinfo, "You need to set the --json, --out  and --chrinfo arguments"


    bwfoler = path.abspath(args.out)

    # make the output folder
    if not path.exists(bwfoler):
        makedirs(bwfoler)
    else:
        print('\n\n\n WARNING: BW FOLDER "{}" ALREADY EXISTS \n\n\n'.format(bwfoler))   
        name = input("Type 'okay' to continue anyway")
        if not name == 'okay':
            sys.exit()
    try:
        with open(args.json) as json_data:
            d = json.load(json_data)
    except Exception as ex:
        print(ex)
        sys.exit("I could not load the json file.")

    condc = 0
    for stagen,fps in d.items():
        condc += len(fps)

    bwD = {}
    progress_count = 1
    for stagen,fps in d.items():
        bwD[stagen] = []
        for en,fp in enumerate(fps):
            ld = {}
            
            print("\r Working on step {}/{}".format(progress_count, condc*3), end='')
            bw_file_prefix = "{}_rep{}_FrC1".format(stagen, en,)
            bw_on = path.join(bwfoler, bw_file_prefix)
            b2bwcomm = ["-p", str(args.procs),"-c",args.chrinfo,"-sh","0","-exts","0","-split","-tlu","100","-q","5","-b",fp,"-o",bw_on]
            if args.verbose:
                b2bwcomm.append("-vv")
            b2bw_parser = b2bw_arg_parser()
            b2bw_args = b2bw_parse_args(b2bw_parser, b2bwcomm )
            bamtobw(b2bw_args)
            ld['FrC1'] = { 'n' : "{}_nstrand.bw".format(bw_file_prefix),
                                   'p' : "{}_pstrand.bw".format(bw_file_prefix)}
            progress_count += 1

            print("\r Working on step {}/{}".format(progress_count, condc*3), end='')
            bw_file_prefix = "{}_rep{}_FrC2".format(stagen, en,)
            bw_on = path.join(bwfoler, bw_file_prefix)
            b2bwcomm = ["-p", str(args.procs),"-c",args.chrinfo,"-sh","0","-exts","0","-split","-tll","101", "-tlu", "160","-q","5","-b",fp,"-o",bw_on]
            if args.verbose:
                b2bwcomm.append("-vv")
            b2bw_parser = b2bw_arg_parser()
            b2bw_args = b2bw_parse_args(b2bw_parser, b2bwcomm )
            bamtobw(b2bw_args)
            ld['FrC2'] = { 'n' : "{}_nstrand.bw".format(bw_file_prefix),
                                   'p' : "{}_pstrand.bw".format(bw_file_prefix)}
            progress_count += 1

            print("\r Working on step {}/{}".format(progress_count, condc*3), end='')
            bw_file_prefix = "{}_rep{}_FrC3".format(stagen, en,)
            bw_on = path.join(bwfoler, bw_file_prefix)
            b2bwcomm = ["-p", str(args.procs),"-c",args.chrinfo,"-sh","0","-exts","0","-split","-tll","161","-q","5","-b",fp,"-o",bw_on]
            if args.verbose:
                b2bwcomm.append("-vv")
            b2bw_parser = b2bw_arg_parser()
            b2bw_args = b2bw_parse_args(b2bw_parser, b2bwcomm )
            bamtobw(b2bw_args)
            ld['FrC3'] = { 'n' : "{}_nstrand.bw".format(bw_file_prefix),
                                   'p' : "{}_pstrand.bw".format(bw_file_prefix)}
            bwD[stagen].append(ld)
            progress_count += 1
    
    with open( path.join(bwfoler, "index.json"), 'w') as outfile:
        json.dump(bwD, outfile)

    return bwD




















# from .nimrod import mkchunks_manager
def mkchunks_manager(theargs):
    """
    One of the preprocessing steps, takes a dataframe and a folder of bigwigs
    and produces the numpy arrays that will be loaded from sha

    """
    # Some Loading and Preprocessing:
    #######################################################################################################################################
    # handle the output folder:
    theargs.savefolder = do_the_output_folder(theargs)
    
    genomeD = load_genome(theargs)   
    assert_bw_dict(theargs.bwf)
    theargs.chromsizeD = { k: len(v) for k,v in genomeD.items()}
    # load the dataframes:
    theargs.test = True
    df_train, df_valid, df_test = load_datadf(theargs)
    make_chunks_sub(theargs, df_valid, worker0, genomeD, name='validation' )
    make_chunks_sub(theargs, df_train, worker066, genomeD, name='train' )
    make_chunks_sub(theargs, df_test, worker0, genomeD, name='test' )



# from .nimrod import training_manager
import warnings
def training_manager(theargs):
    """
    The big training function
    """
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        import tensorflow as tf
    # Some Loading and Preprocessing:
    #######################################################################################################################################
    # handle the output folder:
    theargs.savefolder = do_the_output_folder(theargs)
    
    # make sure the bwf paths are in order:
    # some arg parsing
    # theargs.cache_train, theargs.cache_validation, theargs.cache_test =  (bool(int(x)) for x in theargs.cache_chunks)
    model_state_save_string = path.join(theargs.savefolder, "model_states","model_state_at")
    model_state_restore_string = path.join(theargs.savefolder, "model_states")

    # if (theargs.ndata == "JUSTSAVE") or (theargs.ndata == "NOPE"):
    #     genomeD = load_genome(theargs)   
    #     assert_bw_dict(theargs.bwf)
    #     theargs.chromsizeD = { k: len(v) for k,v in genomeD.items()}
    #     # load the dataframes:
    #     df_train, df_valid, df_test = load_datadf(theargs)
    #     make_chunks_sub(theargs, df_valid, worker0, genomeD, name='validation' )
    #     make_chunks_sub(theargs, df_train, worker066, genomeD, name='train' )
    #     make_chunks_sub(theargs, df_test, worker0, genomeD, name='test' )

    #     del genomeD
    #     if theargs.ndata == "JUSTSAVE":
    #         sys.exit("done!")
    #     else:
    #         activate_chunks(theargs.savefolder, 'validation')
    #         activate_chunks(theargs.savefolder, 'train')
    #         activate_chunks(theargs.savefolder, 'test')

    # else: 
    df_train, df_valid, df_test = load_premade_df(folder=theargs.ndata, args=theargs)

    activate_chunks(theargs.ndata, 'validation')
    activate_chunks(theargs.ndata, 'train')
    activate_chunks(theargs.ndata, 'test')
    
    
    #data managers
    validation_chM = ChunkManager(1000, False, df_valid, minibatch=False)
    train_chM = ChunkManager(100,theargs.bsm, df_train, minibatch=True)
    test_chM = ChunkManager(1000,False, df_test, minibatch=False)

    print_train_report(theargs,train_chM)
    # load the model
    themodel = get_the_model(theargs)
    # class weights:
    class_weights = theargs.weights


    # The actual training of the model
    #######################################################################################################################################
    ustep = 0
    best_loss = 10e25
    best_auc = 0
    time_without_auc_improvement = 0
    time_without_loss_improvement = 0
    saving_flag = False
    log_file_handle = open( path.join(theargs.savefolder ,"training.log") , "w")

    

    # validation_arrays = 'validation')
    # give_me_chunks( validation_arrays, validation_chM.chunk_size, minibatch = False)
    # del validation_arrays
    
    # give_me_chunks( train_arrays, train_chM.chunk_size, minibatch = train_chM.make_a_minibatch_index() )
    # del train_arrays


    with tf.Session() as sess:

        with open( path.join(theargs.savefolder ,"training.log") , "w") as log_file_handle:
            
            # just once:
            sess.run(tf.global_variables_initializer())
            loss_saver = tf.train.Saver(max_to_keep=2)
            auc_saver = tf.train.Saver(max_to_keep=2)
            final_saver = tf.train.Saver(max_to_keep=1)

            hdr = theargs.not_float_printables + theargs.float_printables
            print()
            print()
            print( *hdr, sep='\t', flush=True)
            print( *hdr, sep='\t', flush=True, file=log_file_handle,)

            # the termination flag seems better than just returning from inside this function..
            termination_flag=False
            for epoch in range(theargs.epochs):
                if not termination_flag:
                    
                    for batch in give_me_chunks( "train", train_chM.chunk_size, minibatch = train_chM.make_a_minibatch_index() ):

                        _peakui, batch_labels, batch_atacs, batch_seqs = batch
                        
                        themodel.train_step.run(
                            feed_dict={ 
                                  themodel.atac_inp:    batch_atacs,
                                  themodel.seq_inp:     batch_seqs,
                                  themodel.labels_inp : batch_labels,
                                  themodel.FC2_keep :   0.5,
                                  themodel.class_weights : [ class_weights ]
                                   },
                            session=sess)
                        ustep += 1  


                        if ustep % theargs.valfre == 0:
                            # RUN VALIDATION
                            # validation_arrays = 'validation')
                            VALD = run_test(sess,themodel, 
                                                give_me_chunks_withchange( "validation", validation_chM.chunk_size), 
                                                class_weights )
                            
                            
                            if (VALD['loss'] < best_loss):
                                best_loss = VALD['loss']
                                time_without_loss_improvement = 0
                                loss_saver.save(sess, model_state_save_string[:-2]+'loss_at', global_step=ustep)

                                # if VALD['auc'] > 0.9:
                                #     TESTD = run_test(sess,themodel, 
                                #                     give_me_chunks_withchange( "test", test_chM.chunk_size), 
                                #                     class_weights )
                                #     report_test(TESTD, ">>", "TEST>>", log_file_handle, theargs, saving=False)
                                    
                                #     df = pd.DataFrame()
                                #     df['peak_ui'] = TESTD['peak_ui_vector']
                                #     df['Nimrod_score'] = TESTD['score_']
                                #     df['iT'] = TESTD['labels_vector']

                                #     dffp = path.join(theargs.savefolder, "test_results", 'test_df_at_ustep_{}.tsv'.format(ustep)) 
                                #     df.to_csv(dffp,sep='\t',index=False)

                                #     del TESTD, df

                            else:
                                time_without_loss_improvement += 1

                            if (VALD['auc'] > best_auc):
                                best_auc = VALD['auc']
                                time_without_auc_improvement = 0
                                auc_saver.save(sess, model_state_save_string[:-2]+'auc_at', global_step=ustep)
                            else:
                                time_without_auc_improvement += 1

                            
                            if (time_without_loss_improvement==0) or (time_without_auc_improvement==0):
                                saving_flag=True
                            else:
                                saving_flag=False

                            report_test(VALD, epoch, ustep, log_file_handle, theargs, saving=saving_flag)


                            if (time_without_loss_improvement >= theargs.patience) and (time_without_auc_improvement>= theargs.patience):
                                termination_flag = True
                                break

                            if (VALD['auc'] < 0.6) and (time_without_loss_improvement >= 30):
                                print("this ain't working")
                                termination_flag = True
                                break

                            del VALD


                else:
                    # if termination_flag is set, we break out of the epochs loop
                    break
            
            #Save the last state of the model
            final_saver.save(sess, model_state_save_string[:-2]+'lastState',global_step=ustep)

            #Anything to do after we finish training and before we let the pool and session close?
            delete_chunks('train')
            delete_chunks('validation')
            delete_chunks('test')





# from .nimrod import prepare_test_fromargs
def prepare_test_fromargs(theargs):
    """
    nimrod test
    """
    import tensorflow as tf
    from glob import glob
    
    tempFolder,modelbasename, theargs.savefolder = handle_folders_for_test(theargs)    

    genomeD = load_genome(theargs)   
    theargs.chromsizeD = { k: len(v) for k,v in genomeD.items()}
    
    # load the dataframes:
    df_train, df_valid, df_test = load_datadf(theargs, case="test")
    
    make_chunks_sub(theargs, df_test, worker0, genomeD, name='test' )
    activate_chunks(theargs.savefolder, 'test')
    del genomeD
    
    test_chM = ChunkManager(1000,False, df_test, minibatch=False)
    
    theargs.model=None
    theargs.eds = 1
    theargs.slr=0.00001
    themodel = get_the_model(theargs)
    class_weights = theargs.weights    

    metapath = path.join(tempFolder,modelbasename) +".meta"
    # imported_meta = tf.train.import_meta_graph( metapath )      
    saver = tf.train.Saver()

    
    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.333)
    # sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))

    with tf.Session(config=tf.ConfigProto(gpu_options=gpu_options)) as sess:
        with open( path.join(theargs.savefolder ,"training.log") , "w") as log_file_handle:
            saver.restore(sess, tf.train.latest_checkpoint(tempFolder))
            # sess.run(tf.global_variables_initializer())
                       
            TESTD = run_test(sess,themodel, 
                                    give_me_chunks_withchange( "test", test_chM.chunk_size), class_weights )
            
            report_test(TESTD, 0, 'TEST', log_file_handle, theargs, saving=False, case='test')
           
    
    # vdf = pd.DataFrame()
    # vdf['peak_ui'] = TESTD['peak_ui_vector']
    # vdf['labels_0'] = TESTD['labels'][:,0]
    # vdf['labels_1'] = TESTD['labels'][:,1]  

    foo = df_test.copy()
    foo['logits_0'] = TESTD['logits'][:,0]
    foo['logits_1'] = TESTD['logits'][:,1]
    foo.to_csv("{}/test_df_with_nimrod_scores.tsv".format(theargs.savefolder ), sep='\t')

    # AT THE END:
    # shutil.rmtree(tempFolder)
    delete_chunks('test')


# from .nimrod import continue_training_manager
def continue_training_manager(theargs):
    import tensorflow as tf
    # Some Loading and Preprocessing:
    #######################################################################################################################################
    
    # handle the output folder:
        
    ## TO LOAD A PRETRAINED MODEL
    tempFolder,modelbasename, theargs.savefolder = handle_folders_for_test(theargs)    
    metapath = path.join(tempFolder,modelbasename) +".meta"


    # make sure the bwf paths are in order:
    # some arg parsing
    # theargs.cache_train, theargs.cache_validation, theargs.cache_test =  (bool(int(x)) for x in theargs.cache_chunks)
    model_state_save_string = path.join(theargs.savefolder, "model_states","model_state_at")
    model_state_restore_string = path.join(theargs.savefolder, "model_states")

    
    df_train, df_valid, df_test = load_premade_df( folder=theargs.ndata, args=theargs )

    activate_chunks(theargs.ndata, 'validation')
    activate_chunks(theargs.ndata, 'train')
    activate_chunks(theargs.ndata, 'test')
    
    
    #data managers
    validation_chM = ChunkManager(1000, False, df_valid, minibatch=False)
    train_chM = ChunkManager(100,theargs.bsm, df_train, minibatch=True)
    test_chM = ChunkManager(1000,False, df_test, minibatch=False)

    print_train_report(theargs,train_chM)
    # load the model
    themodel = get_the_model(theargs)
    # class weights:
    class_weights = theargs.weights


    # The actual training of the model
    #######################################################################################################################################
    ustep = 0
    best_loss = 10e25
    best_auc = 0
    time_without_auc_improvement = 0
    time_without_loss_improvement = 0
    saving_flag = False
    log_file_handle = open( path.join(theargs.savefolder ,"training.log") , "w")

    


    saver = tf.train.Saver()

    with tf.Session() as sess:

        with open( path.join(theargs.savefolder ,"training.log") , "w") as log_file_handle:
            

            # just once:
            # sess.run(tf.global_variables_initializer())
            saver.restore(sess, tf.train.latest_checkpoint(tempFolder))
            
            loss_saver = tf.train.Saver(max_to_keep=2)
            auc_saver = tf.train.Saver(max_to_keep=2)
            final_saver = tf.train.Saver(max_to_keep=1)

            hdr = theargs.not_float_printables + theargs.float_printables
            print()
            print()
            print( *hdr, sep='\t', flush=True)
            print( *hdr, sep='\t', flush=True, file=log_file_handle,)

            # the termination flag seems better than just returning from inside this function..
            termination_flag=False
            for epoch in range(theargs.epochs):
                if termination_flag:
                    break
                    
                for batch in give_me_chunks( "train", train_chM.chunk_size, minibatch = train_chM.make_a_minibatch_index() ):

                    _peakui, batch_labels, batch_atacs, batch_seqs = batch
                    
                    themodel.train_step.run(
                        feed_dict={ 
                              themodel.atac_inp:    batch_atacs,
                              themodel.seq_inp:     batch_seqs,
                              themodel.labels_inp : batch_labels,
                              themodel.FC2_keep :   0.5,
                              themodel.class_weights : [ class_weights ]
                               },
                        session=sess)
                    ustep += 1  


                    if ustep % theargs.valfre == 0:
                        # RUN VALIDATION
                        # validation_arrays = 'validation')
                        VALD = run_test(sess,themodel, 
                                            give_me_chunks_withchange( "validation", validation_chM.chunk_size), 
                                            class_weights )
                        
                        
                        if (VALD['loss'] < best_loss):
                            best_loss = VALD['loss']
                            time_without_loss_improvement = 0
                            loss_saver.save(sess, model_state_save_string[:-2]+'loss_at', global_step=ustep)

                            # if VALD['auc'] > 0.9:
                            #     TESTD = run_test(sess,themodel, 
                            #                     give_me_chunks_withchange( "test", test_chM.chunk_size), 
                            #                     class_weights )
                            #     report_test(TESTD, ">>", "TEST>>", log_file_handle, theargs, saving=False)
                                
                            #     df = pd.DataFrame()
                            #     df['peak_ui'] = TESTD['peak_ui_vector']
                            #     df['Nimrod_score'] = TESTD['score_']
                            #     df['iT'] = TESTD['labels_vector']

                            #     dffp = path.join(theargs.savefolder, "test_results", 'test_df_at_ustep_{}.tsv'.format(ustep)) 
                            #     df.to_csv(dffp,sep='\t',index=False)

                            #     del TESTD, df

                        else:
                            time_without_loss_improvement += 1

                        if (VALD['auc'] > best_auc):
                            best_auc = VALD['auc']
                            time_without_auc_improvement = 0
                            auc_saver.save(sess, model_state_save_string[:-2]+'auc_at', global_step=ustep)
                        else:
                            time_without_auc_improvement += 1

                        
                        if (time_without_loss_improvement==0) or (time_without_auc_improvement==0):
                            saving_flag=True
                        else:
                            saving_flag=False

                        report_test(VALD, epoch, ustep, log_file_handle, theargs, saving=saving_flag)


                        if (time_without_loss_improvement >= theargs.patience) and (time_without_auc_improvement>= theargs.patience):
                            termination_flag = True
                            break

                        if (VALD['auc'] < 0.6) and (time_without_loss_improvement >= 30):
                            print("this ain't working")
                            termination_flag = True
                            break

                        del VALD


                
            
            #Save the last state of the model
            final_saver.save(sess, model_state_save_string[:-2]+'lastState',global_step=ustep)

            #Anything to do after we finish training and before we let the pool and session close?
            delete_chunks('train')
            delete_chunks('validation')
            delete_chunks('test')        


