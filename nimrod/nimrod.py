from multiprocessing import Pool
import datetime
import pickle
import shutil
from glob import glob
from os import path,makedirs 
import sys 
import gc



from .helpers import *
# from .helpers import load_genome,print_train_report,get_the_model
# from .helpers import lite_load_datadf,load_datadf,load_premade_df
# from .helpers import make_dir_safely,do_the_output_folder,assert_bw_dict
from .chunklib import make_chunks_sub,activate_chunks
from .chunklib import ChunkManager,give_me_chunks,give_me_chunks_withchange,attach_chunks,delete_chunks


def run_test(sess,themodel, chunks, weights):
    import resource

    m = np.ones((2,2))
    lolog = []
    lolab = []
    losco = []
    lowei = []
    v_loss_sum = 0
    v_loss_mean = 0

    peakui_vector = []

    n = 0
    nr = 0
    for ichunk,ccchunk in enumerate(chunks): 
        n += 1

        _peakui, batch_labels, batch_atacs, batch_seqs = ccchunk 
        peakui_vector += list(_peakui)

        l_comboL1, l_sm_score, l_log, sloss_mean,it_says_learning_rate = sess.run([themodel.h_comboL1, themodel.sm_score,themodel.y_conv, themodel.cross_entropy, themodel.learning_rate],             
            feed_dict={ 
                          themodel.atac_inp: batch_atacs,
                          themodel.seq_inp: batch_seqs,
                          themodel.labels_inp : batch_labels,
                          themodel.FC2_keep:1,
                          themodel.class_weights : [weights] #list in a list: [[1., 1.]]
                           })
        
        isaid = np.argmax(l_log, 1)
        itwas = np.argmax(batch_labels, 1)
        np.add.at(m, [itwas,isaid], 1)
        v_loss_mean += sloss_mean
        
        lolog.append(l_log)
        lolab.append(batch_labels)
        losco.append(l_sm_score)
        # lowei.append(l_comboL1)

        # mygetsize = lambda x: sum([j.nbytes for j in x])
        # print('\r Iteration ', ichunk, ' maxrss: ', resource.getrusage(resource.RUSAGE_SELF).ru_maxrss, flush=True)
        # print("{} {} {} {}".format(mygetsize(lolog),mygetsize(lolab),mygetsize(losco),mygetsize(lowei)))
        # gc.collect()

    v_loss_mean = v_loss_mean/n
    lolog = np.concatenate(lolog)
    lolab = np.concatenate(lolab)
    losco = np.concatenate(losco)    
    # lowei = np.concatenate(lowei)    
    score_vector = (losco[:,1] - losco[:,0]).reshape(-1, 1)
    truth_vector = np.argmax(lolab,1).reshape(-1, 1)

    

    return {
            'model_learning_rate':it_says_learning_rate,
            'conf_line': confusion_matrix_to_line(m),
            'loss' :v_loss_mean ,
            'auc' : ROCAUCSCORE(y_true = truth_vector, 
                                y_score = score_vector) ,
            'aps' : APScore(y_true = truth_vector, 
                                y_score = score_vector) ,
            'sensitivity' : metric_Sensitivity(m) ,
            'specificity' : metric_Specificity(m) ,
            'fpr' : metric_FPR(m),
            'F05' : metric_Fscore(m, 0.5),
            'F1' : metric_Fscore(m, 1),
            'F2' : metric_Fscore(m, 1),
            'Precision' : metric_Precision(m),
            'youden' : metric_youden(m),
            'logits' : lolog,
            'labels' : lolab,
            'peak_ui_vector' :peakui_vector,
            # 'comboL1' : lowei,
            'score_':(losco[:,1] - losco[:,0]),
            'labels_vector' :np.argmax(lolab,1)
              }


def report_test(THEDICT, epoch, ustep, log_file_handle, theargs, case='validation',saving=False, pickledump=False ):
    val_printables = [datetime.datetime.now(), epoch, ustep, int(saving), THEDICT['model_learning_rate']] + [format(THEDICT[x], '.4f') for x in theargs.float_printables]
    print( *val_printables, sep='\t', flush=True)
    print( *val_printables, sep='\t', flush=True, file=log_file_handle,)

    # print the validation results in a file:
    vdf = pd.DataFrame()
    vdf['peak_ui'] = THEDICT['peak_ui_vector']
    vdf['logits_0'] = THEDICT['logits'][:,0]
    vdf['logits_1'] = THEDICT['logits'][:,1]
    vdf['labels_0'] = THEDICT['labels'][:,0]
    vdf['labels_1'] = THEDICT['labels'][:,1]  
    
    valdffilepath = path.join(theargs.savefolder, "test_results", '{}_after_{}_batches.csv'.format(case, ustep)) 

    vdf.to_csv(valdffilepath, sep='\t',index=False)                                    


    # print(THEDICT['comboL1'].shape)
    # print(THEDICT['comboL1'][index_by_best_score].shape)

    if False != pickledump:
        pdumpf = path.join(theargs.savefolder, "pickledump" )
        make_dir_safely(pdumpf)
        
        index_by_best_score = (vdf['logits_1'] - vdf['logits_0']).sort_values(ascending=False).index.values
        index_random = vdf.sample(pickledump).index.values

        pickle.dump( THEDICT['logits'][index_by_best_score[:pickledump]], open("{}/top_{}_logits.pickle".format(pdumpf, pickledump), 'wb'))
        pickle.dump( THEDICT['comboL1'][index_by_best_score[:pickledump]] , open("{}/top_{}_comboL1.pickle".format(pdumpf, pickledump), 'wb'))

        pickle.dump( THEDICT['logits'][index_random], open("{}/{}_logits.pickle".format(pdumpf, "random"), 'wb'))
        pickle.dump( THEDICT['comboL1'][index_random] , open("{}/{}_comboL1.pickle".format(pdumpf, "random"), 'wb'))



    return vdf    

def handle_folders_for_test(theargs):   
    savefolder = path.abspath( theargs.out )
    make_dir_safely(savefolder)
    makedirs( path.join(savefolder, "test_results"))
    tempFolder = savefolder+"/the_model_as_loaded/"
    makedirs(tempFolder)
    
    for fp in glob( path.abspath(theargs.modelP)+"*"):
        shutil.copy(fp , tempFolder )
    
    modelbasename = path.basename(theargs.modelP)
    with open(tempFolder+"checkpoint", 'w') as fo:
        l1 = 'model_checkpoint_path: "{}"\n'.format( path.join(tempFolder, modelbasename) )
        l2 = 'all_model_checkpoint_paths: "{}"'.format( path.join(tempFolder, modelbasename) )
        fo.write(l1)
        fo.write(l2)   

    return tempFolder,modelbasename, savefolder




from .chunklib import ChunkManager,give_me_chunks,give_me_chunks_withchange

def manual_lr_manager(theargs):
    print("nimrod.manual_lr_manager ")
    import tensorflow as tf
    # Some Loading and Preprocessing:
    #######################################################################################################################################
    # handle the output folder:
    theargs.savefolder = do_the_output_folder(theargs)
    
    # make sure the bwf paths are in order:
    # some arg parsing
    # theargs.cache_train, theargs.cache_validation, theargs.cache_test =  (bool(int(x)) for x in theargs.cache_chunks)
    model_state_save_string = path.join(theargs.savefolder, "model_states","model_state_at")
    model_state_restore_string = path.join(theargs.savefolder, "model_states")

    if (theargs.prep == "JUSTSAVE") or (theargs.prep == "NOPE"):
        genomeD = load_genome(theargs)   
        assert_bw_dict(theargs.bwf)
        theargs.chromsizeD = { k: len(v) for k,v in genomeD.items()}
        # load the dataframes:
        df_train, df_valid, df_test = load_datadf(theargs)
        make_chunks_sub(theargs, df_valid, worker0, genomeD, name='validation' )
        make_chunks_sub(theargs, df_train, worker066, genomeD, name='train' )
        make_chunks_sub(theargs, df_test, worker0, genomeD, name='test' )

        del genomeD
        if theargs.prep == "JUSTSAVE":
            sys.exit("done!")
        else:
            activate_chunks(theargs.savefolder, 'validation')
            activate_chunks(theargs.savefolder, 'train')
            activate_chunks(theargs.savefolder, 'test')

    else: 
        df_train, df_valid, df_test = load_premade_df( folder=theargs.prep, args=theargs )

        activate_chunks(theargs.prep, 'validation')
        activate_chunks(theargs.prep, 'train')
        activate_chunks(theargs.prep, 'test')
    
    
    #data managers
    # validation_chM = ChunkManager(1000, False, df_valid, minibatch=False)
    train_chM = ChunkManager(100,theargs.bsm, df_train, minibatch=True)
    # test_chM = ChunkManager(1000,False, df_test, minibatch=False)

    print_train_report(theargs,train_chM)
    # load the model
    themodel = get_the_model(theargs,alt=True)
    # class weights:
    class_weights = theargs.weights


    # The actual training of the model
    #######################################################################################################################################
    ustep = 0
    best_loss = 10e25
    best_auc = 0
    time_without_auc_improvement = 0
    time_without_loss_improvement = 0
    saving_flag = False
    log_file_handle = open( path.join(theargs.savefolder ,"training.log") , "w")

    

    # validation_arrays = attach_chunks('validation')
    # give_me_chunks( validation_arrays, validation_chM.chunk_size, minibatch = False)
    # del validation_arrays
    
    # give_me_chunks( train_arrays, train_chM.chunk_size, minibatch = train_chM.make_a_minibatch_index() )
    # del train_arrays


    with tf.Session() as sess:

        with open( path.join(theargs.savefolder ,"training.log") , "w") as log_file_handle:
            

            # just once:
            sess.run(tf.global_variables_initializer())
            # loss_saver = tf.train.Saver(max_to_keep=2)
            # auc_saver = tf.train.Saver(max_to_keep=2)
            # final_saver = tf.train.Saver(max_to_keep=1)

            # hdr = theargs.not_float_printables + theargs.float_printables
            # print()
            # print()
            # print( *hdr, sep='\t', flush=True)
            # print( *hdr, sep='\t', flush=True, file=log_file_handle,)

                  
            lrate=1e-8

            for epoch in range(theargs.epochs):
                print("epoch:", epoch)

                for batch in give_me_chunks( "train", train_chM.chunk_size, minibatch = train_chM.make_a_minibatch_index() ):


                    _peakui, batch_labels, batch_atacs, batch_seqs = batch


                    trainstep,theloss = sess.run( [themodel.train_step, themodel.cross_entropy],
                        feed_dict={ 
                              themodel.atac_inp:    batch_atacs,
                              themodel.seq_inp:     batch_seqs,
                              themodel.labels_inp : batch_labels,
                              themodel.FC2_keep :   0.5,
                              themodel.class_weights : [ class_weights ],

                              themodel.learning_rate: lrate,
                               })
                    
                    
                    if ustep % theargs.valfre == 0:
                        # print()
                        print(ustep, lrate, theloss)
                        lrate *= 1.05

                    # print('\r ustep:{}'.format(ustep))
                    ustep += 1  

                

        

            #Anything to do after we finish training and before we let the pool and session close?
            delete_chunks('train')
            delete_chunks('validation')
            delete_chunks('test')

            
                        

    # # We let the pool and session close and we'll restart them for the test
    # # for clarity's sake
    # # If the df_test is not None
    # if (pd.core.frame.DataFrame == type(df_test)) and theargs.test:                 
    #     test_manager(theargs, themodel,





