import os, sys
# that's the module where we describe the argparsing interface
from .arg_parser import parse_arguments
# the functions are kept there to keep main clean
from .arg_parser_funcs import *



def main(take_arguments=False,passed_arguments=None):
    """
    This is the first thing that launches when nimrod is called

    Argparse offers a much more elegant way to set a default function
    from the arguments with set_default. BUT I could not manage to 
    run the --help without loading TensorFlow which makes a lot of noise
    and ultimately makes running nimrod annoying.
    So I use this seemingly archaic elif structure because it works good enough
    It could probably be done with set_default, but let's not waste time over something
    that already works

    The organization of the code is not spectacular.
    arg_parser_funcs contains all the main functions that are called through the command line
    It imports semi-randomly from helpers, nimrod, chunklib and b2bw
    chunklib should contain all the data-chunk-handling functions
    nimrod should contain NN-related things
    helpers has a bit of everything
    b2bw contains the bam to bigwig code

    """
    # we allow the option to call the main function with fake argument object to run tests
    if not take_arguments:
        args = parse_arguments()
    else:
        args = passed_arguments

    if args.myfunc == 'print':    
        print(args)
    elif args.myfunc == 'training_manager':
        training_manager(args)
    elif args.myfunc == 'continue_training_manager':
        continue_training_manager(args) 
    elif args.myfunc == 'mkchunks_manager':
        mkchunks_manager(args)
    elif args.myfunc == 'prepare_test_fromargs':
        prepare_test_fromargs(args)
    elif args.myfunc == 'make_bws':
        make_bws(args)
    elif args.myfunc == 'mkgenomeindex':
        mkgenomeindex(args)
    elif args.myfunc == 'printjson':
        printjson(args)
    elif args.myfunc == 'kowalski':
        kowalski(args)
    elif args.myfunc == 'manual_lr_manager':
        manual_lr_manager(args)
    elif args.myfunc == 'makedataset':
        makedataset(args)
    elif args.myfunc == 'mk_chrominfo':
        mk_chrominfo(args)
    elif args.myfunc == 'moods2tsv':
        moods2tsv(args)
    elif args.myfunc == 'pass':
        return
    else:
        sys.exit("(O . O) <[Did you forget adding something in __main__.py ?]")   

if __name__ == "__main__":
    main()