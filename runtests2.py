import unittest
import traceback
from os import path,getcwd
import sys

# import nimrod's main function so we can test it
from nimrod.__main__ import main as nimain

import warnings
import zipfile


from nimrod.arg_parser_funcs import *
class O():
    pass


# from https://www.neuraldump.net/2017/06/how-to-suppress-python-unittest-warnings/
# a wrapper to ignore warnings in unittesting
def ignore_warnings(test_func):
    def do_test(self, *args, **kwargs):
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            # warnings.filterwarnings("ignore", category=ResourceWarning)
            test_func(self, *args, **kwargs)
    return do_test



class Test_prepare_test_fromargs(unittest.TestCase):
    def __init__(self, methodName="runTest"):
        unittest.TestCase.__init__(self, methodName)
        self.args = O()
        self.TEMPDIR = tempfile.TemporaryDirectory()

        self.args.genome = path.join(RUNNINGDIR,"test_data","test_hg38.pickle")         
        self.args.df =  path.join(RUNNINGDIR,"test_data","test_dataset.tsv") 
        self.args.bwf = path.join(RUNNINGDIR,"test_data","test_bw_folder") 
        self.args.out = path.join(self.TEMPDIR.name, 'test_testresultsfolder')


        self.args.modelPdir = path.join(RUNNINGDIR, 'test_data','model')
        self.args.modelP = path.join(self.args.modelPdir,'model_state_lastState-2115000' )

        

        self.args.weights = [1., 1.]
        self.args.win_size = 1000
        self.args.cache_chunks = '010'
        self.args.test = True
        self.args.not_float_printables = ['time','epoch','ustep','willSaveModelState','lrnRate']
        self.args.float_printables = ['loss', 'auc','aps','F05','F1','F2','Precision','youden', 'sensitivity', 'specificity', 'fpr']

    def __del__(self):
        # rezip the model files to save some space
        self.TEMPDIR.cleanup()
    
    # def unzip_file(self, target_file, target_folder):
    #     with zipfile.ZipFile(target_file, 'r') as zip_ref:
    #         zip_ref.extractall(target_folder)
    # def zip_file(self, target_file):
    #     # writing files to a zipfile 
    #     with zipfile.ZipFile(target_file,'w') as zip: 
    #         zip.write(target_file[:-3]) 

    @ignore_warnings   
    def test_thatItLoads(self):
        try:
            prepare_test_fromargs(self.args)
        except Exception as e:
            self.fail(f"prepare_test_fromargs failed to load unexpectedly!\n\n{traceback.format_exc()}")
        pass





if __name__ == '__main__':
    import tempfile

    RUNNINGDIR = getcwd()
    
    unittest.main()


    

    