import os
from setuptools import setup

# This little script sets up how our package is installed in the system
# entry_points is important, that's what answers when a user calls 'nimrod' inthe terminal


# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "nimrod",
    version = "0.9",
    author = "Panos Firbas",
    author_email = "panosfirbas@gmail.com",
    description = ("An Artificial Neural Network that reads genomic sequence and ATAC-seq signal to predict Transcription-Factor binding sites."),
    license = "GPL3",
    keywords = "bioinformatics NeuralNetwork TensorFlow TranscriptionFactors",
    url = "https://gitlab.com/panosfirbas/nimrod",
    packages=['nimrod'],
    # install_requires=['numpy', 'pysam', 'pyBigWig', 'SharedArray'],    
    long_description=read('README.md'),
    entry_points={
          'console_scripts': [
              'nimrod = nimrod.__main__:main'
          ]
      },
    classifiers=[
        "Development Status :: Beta",
        "Topic :: Utilities"
    ],
)
